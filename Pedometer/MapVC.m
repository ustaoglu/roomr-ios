//
//  MapVC.m
//  Pedometer
//
//  Created by Efdal Ustaoglu on 29.03.18.
//  Copyright © 2018 Navindo GmbH. All rights reserved.
//

#import "MapVC.h"

@interface MapVC () {
    
    BOOL firstLocationUpdate;
    NSDictionary *geoJSON;
    NSMutableArray *geoJsonArray;
    int drawStyle;
    LatLngWithTags *startingPoint;
    LatLngWithTags *destiPoint;
    UISegmentedControl *_switcher;
    GMSTileLayer *_tileLayer;
    NSInteger _floor;
    CLLocation *userLocation;
    LocationUtils *locUtil;
    
    NSMutableArray * targetPoints;
    NSMutableArray * targetPointsTagged;
    NSMutableArray * routablePath;

    UIButton *button1;
    UIButton *button2;
    UIButton *button3;
    UIButton *button4;
    UIButton *button5;
    
    NSString *tileLevel;
    UITextField *topTextField;
    
    GMUGeometryRenderer *renderer;
    GMSPolyline *previousRouteLine;
    LocationDetailView *_overlay;
    NSString *destinationNameBottomString;
    NSString *destinationAddrBottomString;
    NSMutableArray *markerList;
    NSMutableArray *receivedPolylineArray;
    CSTileLayer *cslayer;
    float oldZoom;
    
    BuildingBox *MI_Building;
    BuildingBox *MunFreiheit;
    BuildingBox *MW_Building;
    BuildingBox *MC_Building;
    UIImage *cancelImage;
    UIButton *cancelButton;
    NSNotification *receivedNotification;
    Room *receivedDestinationRoom;
    BOOL updateStartingPoint;
}
@end

static CGFloat kOverlayHeight = 140.0f;

@implementation MapVC
@synthesize bottomLeftView, bottomLeftButton, bottomLeftImageView, geoJSONDijkstra;
@synthesize mapView, wmsTileLayer;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self loadView];
    
    updateStartingPoint = NO;
    
    //[self initView];
    [self geoJsonParse];
    [self initFloorButtons];
    [self initBuildingBox];
    // init current position to ground floor
    
    [mapView clear];//Clear table for new tile layer
    tileLevel = [NSString stringWithFormat:@"0"];//assign the level
    [self changeColorOfSelectedFloor:button5];//select the button
    [self tileUpdate];
    [self polylineArrayCheck];
    
}

- (void)initBuildingBox {
    
    // Garching MI
    MI_Building = [[BuildingBox alloc] init];
    MI_Building.min_lat = 48.2613;
    MI_Building.max_lat = 48.2636;
    MI_Building.min_lng = 11.6653;
    MI_Building.max_lng = 11.6705;
    
    // Munchner Freiheit metro station
    MunFreiheit = [[BuildingBox alloc] init];
    MunFreiheit.min_lat = 48.160468;
    MunFreiheit.max_lat = 48.162697;
    MunFreiheit.min_lng = 11.584937;
    MunFreiheit.max_lng = 11.587385;
    
    // Machninenwesen
    MW_Building = [[BuildingBox alloc] init];
    MW_Building.min_lat = 48.264547;
    MW_Building.max_lat = 48.266825;
    MW_Building.min_lng = 11.667344;
    MW_Building.max_lng = 11.671324;
    
    //Main Campus
    MC_Building = [[BuildingBox alloc] init];
    MC_Building.min_lat = 48.147561;
    MC_Building.max_lat = 48.151462;
    MC_Building.min_lng = 11.565581;
    MC_Building.max_lng = 11.570093;
    
}

- (void)loadView {
    markerList = [[NSMutableArray alloc] init];
    // Create a GMSCameraPosition that tells the map to display
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:48.1765219
                                                            longitude:11.5925233
                                                                 zoom:16];//IBM Tower
    camera = [GMSCameraPosition cameraWithLatitude:48.262461
                                         longitude:11.668301
                                              zoom:16];//Garching
    mapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    mapView.delegate = self;
    mapView.myLocationEnabled = YES;
    mapView.settings.compassButton = YES;
    mapView.settings.myLocationButton = YES;
    mapView.indoorEnabled = NO;
    //mapView.padding = UIEdgeInsetsMake(0, 0, kOverlayHeight, 0);

    // Listen to the myLocation property of GMSMapView.
    [mapView addObserver:self forKeyPath:@"myLocation" options:NSKeyValueObservingOptionNew context:NULL];
    self.view = mapView;

    UITapGestureRecognizer *tgr = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleGesture:)];
    tgr.numberOfTapsRequired = 1;
    [mapView addGestureRecognizer:tgr];

    // Ask for My Location data after the map has already been added to the UI.
    dispatch_async(dispatch_get_main_queue(), ^{
        mapView.myLocationEnabled = YES;
    });
    
    //[self createMarkerForIBM];
    
    CGRect overlayFrame = CGRectMake(0, -kOverlayHeight, 0, kOverlayHeight);
    _overlay = [[[NSBundle mainBundle] loadNibNamed:@"LocationDetailView" owner:self options:nil] lastObject];
    //[_overlay.bottomSquareButton addTarget:self action:@selector(changeMapRoute) forControlEvents:UIControlEventTouchUpInside];
    _overlay.frame = overlayFrame;
    _overlay.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
    _overlay.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_overlay];
    _overlay.hidden = YES;
    
     // -- method A : WMS tile layer with full control of the cache
    wmsTileLayer  = [[WMSTileLayer alloc] initWithUrl:BASE_TILE_URL];
    wmsTileLayer.tileLevel = tileLevel;
    wmsTileLayer.map = mapView;
    
    //cslayer = [CSTileLayer sharedInstance];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"GMSMapObject" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"drawRouteFromDetail" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"GMSMapArray" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"topSearchDataReceived" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(triggerAction:) name:@"GMSMapObject" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(drawRouteFromDetail:) name:@"drawRouteFromDetail" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(triggerAction2:) name:@"GMSMapArray" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(topSearchDataReceived:) name:@"topSearchDataReceived" object:nil];
    
    if (topTextField.text.length == 0) {
        cancelButton.hidden = YES;
    } else {
        cancelButton.hidden = NO;
    }
}

- (void)viewWillDisappear:(BOOL)animated {
//    [mapView clear];
//    [self setStartingPointMI];
}

- (void)initFloorButtons{
    
    UIColor *buttonBackgroundColor = [UIColor colorWithRed:255 green:255 blue:0.96 alpha:1.0];
    UIColor *buttonTextColor = [UIColor blackColor];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    
    button5 = [[UIButton alloc] initWithFrame:CGRectMake(10, screenRect.size.height - 80, 40, 40)];
    button4 = [[UIButton alloc] initWithFrame:CGRectMake(10, button5.frame.origin.y - 45, 40, 40)];
    button3 = [[UIButton alloc] initWithFrame:CGRectMake(10, button4.frame.origin.y - 45, 40, 40)];
    button2 = [[UIButton alloc] initWithFrame:CGRectMake(10, button3.frame.origin.y - 45, 40, 40)];
    button1 = [[UIButton alloc] initWithFrame:CGRectMake(10, button2.frame.origin.y - 45, 40, 40)];
    
    [button1 setTitle:@"5" forState:UIControlStateNormal];
    [button2 setTitle:@"4" forState:UIControlStateNormal];
    [button3 setTitle:@"3" forState:UIControlStateNormal];
    [button4 setTitle:@"2" forState:UIControlStateNormal];
    [button5 setTitle:@"1" forState:UIControlStateNormal];
    
    button1.titleLabel.font = [UIFont systemFontOfSize:20];
    button2.titleLabel.font = [UIFont systemFontOfSize:20];
    button3.titleLabel.font = [UIFont systemFontOfSize:20];
    button4.titleLabel.font = [UIFont systemFontOfSize:20];
    button5.titleLabel.font = [UIFont systemFontOfSize:20];
    
    [button1 setTitleColor:buttonTextColor forState:UIControlStateNormal];
    [button2 setTitleColor:buttonTextColor forState:UIControlStateNormal];
    [button3 setTitleColor:buttonTextColor forState:UIControlStateNormal];
    [button4 setTitleColor:buttonTextColor forState:UIControlStateNormal];
    [button5 setTitleColor:buttonTextColor forState:UIControlStateNormal];
    
    //make rounded
    button1.layer.cornerRadius = button1.frame.size.height / 2;
    button2.layer.cornerRadius = button2.frame.size.height / 2;
    button3.layer.cornerRadius = button3.frame.size.height / 2;
    button4.layer.cornerRadius = button4.frame.size.height / 2;
    button5.layer.cornerRadius = button5.frame.size.height / 2;
    
    //background
    button1.backgroundColor = buttonBackgroundColor;
    button2.backgroundColor = buttonBackgroundColor;
    button3.backgroundColor = buttonBackgroundColor;
    button4.backgroundColor = buttonBackgroundColor;
    button5.backgroundColor = buttonBackgroundColor;
    
    //initially set them hidden
    button1.hidden = YES;
    button2.hidden = YES;
    button3.hidden = YES;
    button4.hidden = YES;
    button5.hidden = YES;
    
    [button1 addTarget:self action:@selector(button1Action:) forControlEvents:UIControlEventTouchUpInside];
    [button2 addTarget:self action:@selector(button2Action:) forControlEvents:UIControlEventTouchUpInside];
    [button3 addTarget:self action:@selector(button3Action:) forControlEvents:UIControlEventTouchUpInside];
    [button4 addTarget:self action:@selector(button4Action:) forControlEvents:UIControlEventTouchUpInside];
    [button5 addTarget:self action:@selector(button5Action:) forControlEvents:UIControlEventTouchUpInside];
    
    //add to the screen
    [self.view addSubview:button1];
    [self.view addSubview:button2];
    [self.view addSubview:button3];
    [self.view addSubview:button4];
    [self.view addSubview:button5];
    
    // top text bar
    
    topTextField = [[UITextField alloc] initWithFrame:CGRectMake(screenRect.size.width/2 - 278/2, 50, 278, 40)];
    topTextField.backgroundColor = [UIColor whiteColor];
    [topTextField setPlaceholder:@"Search"];
    
    topTextField.borderStyle = UITextBorderStyleRoundedRect;
    topTextField.font = [UIFont systemFontOfSize:15];
    topTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    topTextField.keyboardType = UIKeyboardTypeDefault;
    topTextField.returnKeyType = UIReturnKeyDone;
    topTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    topTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    topTextField.delegate = self;
    [self.view addSubview:topTextField];
    
    UIButton *textFieldButton = [[UIButton alloc] initWithFrame:CGRectMake(screenRect.size.width/2 - 278/2, 50, 278 - 50, 40)]; // (10, 25, screenRect.size.width - 50, 40)
    [textFieldButton addTarget:self action:@selector(textFieldButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:textFieldButton];
    cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(textFieldButton.frame.origin.x + textFieldButton.frame.size.width, 50, 50, 40)];
    
    cancelImage = [UIImage imageNamed:@"cancel"];
    cancelImage = [self imageWithImage:cancelImage scaledToSize:CGSizeMake(20, 20)];
    
    [cancelButton setImage:cancelImage forState:UIControlStateNormal];
    cancelButton.backgroundColor = [UIColor clearColor];
    [cancelButton addTarget:self action:@selector(clearTextField) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cancelButton];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    UIButton *topOverlayButton = [[UIButton alloc] initWithFrame:CGRectMake(50, 50, 90, 44)];
    [topOverlayButton setTitle:@"OVERLAY" forState:UIControlStateNormal];
    [topOverlayButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    //[self.view addSubview:topOverlayButton]; // don't add the button to the view
    //[topOverlayButton addTarget:self action:@selector(didTapFlyIn) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *topMapTypeButton = [[UIButton alloc] initWithFrame:CGRectMake(screenRect.size.width - 150, 50, 90, 44)];
    [topMapTypeButton setTitle:@"Change MAP" forState:UIControlStateNormal];
    [topMapTypeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    //[self.view addSubview:topMapTypeButton]; // don't add the button to the view
    //[topMapTypeButton addTarget:self action:@selector(mapTypeSelection) forControlEvents:UIControlEventTouchUpInside];
    
    if (topTextField.text.length == 0) {
        cancelButton.hidden = YES;
    } else {
        cancelButton.hidden = NO;
    }
}

#pragma mark - Notification
-(void) triggerAction:(NSNotification *) notification {
    previousRouteLine = notification.object;
    NSLog(@"%@",previousRouteLine);
    NSLog(@"%@",previousRouteLine.map);
    NSLog(@"%@",previousRouteLine.path);
}

-(void) triggerAction2:(NSNotification *) notification {
    receivedPolylineArray = notification.object;
    for (int i = 0;i< receivedPolylineArray.count;i++) {
        if ([tileLevel isEqualToString:[NSString stringWithFormat:@"%d",i]]) {
            [receivedPolylineArray[i] setStrokeColor:[UIColor redColor]];
        } else{
            [receivedPolylineArray[i] setStrokeColor:[UIColor grayColor]];
        }
    }
}

//In here after obj received search for starting point
- (void)topSearchDataReceived:(NSNotification *) notification {
    //set the text and then make the routing
    [mapView clear];
    updateStartingPoint = NO;
    receivedNotification = notification;
    Room *receivedDestination = notification.object;
    receivedDestinationRoom = receivedDestination;
    topTextField.text = [receivedDestination room_id];
    [self textFieldShouldReturn:topTextField :YES];
    if (receivedNotification != nil) {
        [self zoomIntoDestination:[markerList lastObject]];
    }
}

- (void)createMarker:(CLLocationCoordinate2D)location :(NSString *)placeTitle{

    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = location;
    marker.title = placeTitle;
    marker.map = mapView;
    [markerList addObject:marker];
}

- (void)geoJsonParse {
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"munchner_freiheit" ofType:@"geojson"];
    NSURL *url = [NSURL fileURLWithPath:path];
    GMUGeoJSONParser *parser = [[GMUGeoJSONParser alloc] initWithURL:url];
    [parser parse];
    
    renderer = [[GMUGeometryRenderer alloc] initWithMap:mapView geometries:parser.features];
    [renderer render];

    //set starting point - way/346040148 /*11.5866166,48.1617971*/
    //provide destination point - way/258139177
    //change the function that returns GSM map
    
    locUtil = [[LocationUtils alloc] init];
    targetPoints = [[NSMutableArray alloc] init];
    targetPointsTagged = [[NSMutableArray alloc] init];
    routablePath = [[NSMutableArray alloc] init];
    locUtil.routablePath = routablePath;
    locUtil.targetPoints = targetPoints;
    locUtil.targetPointsTagged = targetPointsTagged;
    
//    NSURL *URL = [[NSBundle mainBundle] URLForResource:@"indoor_path_ga_mi" withExtension:@"geojson"];
//    NSData *data = [NSData dataWithContentsOfURL:URL];
//    geoJSON = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

    NSURL *URL = [[NSBundle mainBundle] URLForResource:@"mi" withExtension:@"geojson"];
    NSData *data = [NSData dataWithContentsOfURL:URL];
    NSDictionary *dict1 = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    NSURL *URL2 = [[NSBundle mainBundle] URLForResource:@"mw" withExtension:@"geojson"];
    NSData *data2 = [NSData dataWithContentsOfURL:URL2];
    NSDictionary *dict2 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];

    NSURL *URL3 = [[NSBundle mainBundle] URLForResource:@"mc" withExtension:@"geojson"];
    NSData *data3 = [NSData dataWithContentsOfURL:URL3];
    NSDictionary *dict3 = [NSJSONSerialization JSONObjectWithData:data3 options:0 error:nil];
    
    geoJsonArray = [[NSMutableArray alloc] init];
    [geoJsonArray addObject:dict1];
    [geoJsonArray addObject:dict2];
    [geoJsonArray addObject:dict3];
    
    //startingPoint = [locUtil setupStartingPoint:@"48.1617971" :@"11.5866166" :@"-1"];//entrance munchner-freiheit
    [self setStartingPointMI];
    [renderer clearAllMarkers]; //COMMENT-IN
}

- (void)setStartingPointMI {
    startingPoint = [locUtil setupStartingPoint:@"48.26253058447" :@"11.66877060995" :@"0": @"entrance"];//garching entrance mi
    NSLog(@"starting->%@",startingPoint);
}

- (void)setStartingPointMW {
    startingPoint = [locUtil setupStartingPoint:@"48.26567081673" :@"11.67088826523" :@"0": @"entrance"];//garching entrance mw
    NSLog(@"starting->%@",startingPoint);
}

- (void)setStartingPointMC {
    startingPoint = [locUtil setupStartingPoint:@"48.14866569534" :@"11.56859954741" :@"0": @"entrance"];//garching entrance mc
    NSLog(@"starting->%@",startingPoint);
}

- (void)mapView:(GMSMapView *)mapView didTapMyLocation:(CLLocationCoordinate2D)location {
    NSString *message = [NSString stringWithFormat:@"My Location Dot Tapped at: [lat: %f, lng: %f]",
                         location.latitude, location.longitude];
    UIAlertController *alertController =
    [UIAlertController alertControllerWithTitle:@"Location Tapped"
                                        message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action){
                                                     }];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    
    if(topTextField.text.length > 0)
        [self moveOverlayChange];
}

- (void)dealloc {
    [mapView removeObserver:self forKeyPath:@"myLocation" context:NULL];
}

- (void)createPolygon {
    
    // Create the first polygon.
    GMSPolygon *polygon = [[GMSPolygon alloc] init];
    //polygon.path = [self pathOfNewYorkState];
    polygon.title = @"New York";
    //polygon.fillColor = [UIColor colorWithRed:0.25 green:0 blue:0 alpha:0.2f];
    polygon.strokeColor = [UIColor blackColor];
    polygon.strokeWidth = 2;
    polygon.tappable = YES;
    polygon.map = mapView;
}

#pragma mark - KVO updates

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    if (!firstLocationUpdate) {
        // If the first location update has not yet been received, then jump to that
        // location.
        firstLocationUpdate = YES;
        // CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
        // mapView.camera = [GMSCameraPosition cameraWithTarget:location.coordinate
        //                                                 zoom:16];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)handleGesture:(UIGestureRecognizer *)gestureRecognizer {
    NSLog(@"--->");
    if (gestureRecognizer.state != UIGestureRecognizerStateEnded)
        return;
    
    CGPoint touchPoint = [gestureRecognizer locationInView:mapView];
    CLLocationCoordinate2D coordinate = [mapView.projection coordinateForPoint:touchPoint];
    
    NSLog(@"latitude  %f longitude %f",coordinate.latitude,coordinate.longitude);
    
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"" message:@"Do you want me set your position here?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        userLocation = [[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    }];
    UIAlertAction *noButton = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) { }];
    [alert addAction:okButton];
    [alert addAction:noButton];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition*)position {
    
    float zoom = mapView.camera.zoom;
    if (oldZoom == 0)
        oldZoom = zoom;
    NSLog(@"ZOOM->%lf --- LAT->%lf --- LNG->%lf",zoom ,position.target.latitude,position.target.longitude);
    
    if ([[self cameraPositionCheck:position :zoom] valueForKey:@"floorCount"]) {

        int floorCount = [[[self cameraPositionCheck:position :zoom] valueForKey:@"floorCount"] intValue];
        
        NSArray *floorNames = [[[self cameraPositionCheck:position :zoom] valueForKey:@"floorNames"] componentsSeparatedByString:@","];
        
        if (tileLevel == nil) {
            tileLevel = floorNames[0];
        }
        
        button1.hidden = NO;
        button2.hidden = NO;
        button3.hidden = NO;
        button4.hidden = NO;
        button5.hidden = NO;
        
        if (floorCount == 1) {
            
            button1.hidden = YES;
            button2.hidden = YES;
            button3.hidden = YES;
            button4.hidden = YES;
            [button5 setTitle:floorNames[0] forState:UIControlStateNormal];

        } else if (floorCount == 2) {
            
            button1.hidden = YES;
            button2.hidden = YES;
            button3.hidden = YES;
            [button4 setTitle:floorNames[1] forState:UIControlStateNormal];
            [button5 setTitle:floorNames[2] forState:UIControlStateNormal];
        } else if (floorCount == 3) {
            
            button1.hidden = YES;
            button2.hidden = YES;
            [button3 setTitle:floorNames[1] forState:UIControlStateNormal];
            [button4 setTitle:floorNames[2] forState:UIControlStateNormal];
            [button5 setTitle:floorNames[3] forState:UIControlStateNormal];
        } else if (floorCount == 4) {
            
            button1.hidden = YES;
            [button2 setTitle:floorNames[0] forState:UIControlStateNormal];
            [button3 setTitle:floorNames[1] forState:UIControlStateNormal];
            [button4 setTitle:floorNames[2] forState:UIControlStateNormal];
            [button5 setTitle:floorNames[3] forState:UIControlStateNormal];
        } else {
            
            [button1 setTitle:floorNames[0] forState:UIControlStateNormal];
            [button2 setTitle:floorNames[1] forState:UIControlStateNormal];
            [button3 setTitle:floorNames[2] forState:UIControlStateNormal];
            [button4 setTitle:floorNames[3] forState:UIControlStateNormal];
            [button5 setTitle:floorNames[4] forState:UIControlStateNormal];
        }
        
        if (zoom > 15 ) {
            wmsTileLayer.tileLevel = tileLevel;
            wmsTileLayer.map = mapView;
        }
        
        //check location and zoom level
        if ([[[self cameraPositionCheck:position :zoom] valueForKey:@"location"] isEqualToString:@"mi"] && zoom > 18) {
            
            //check if routing is available if so then add it to the stairs
            //[self addStairsMarker:@"48.26253058447" :@"11.66877060995" :CGSizeMake(20, 20) :@"stairs"];
            
//            NSLog(@"receivedPolylineArray:%@",receivedPolylineArray);
//            for (GMSPolyline *i in receivedPolylineArray)
//                NSLog(@"i->%@",i);
            
        } else if(zoom < 15){
            [mapView clear];
        }
        
        //[self tileUpdate];
        if (oldZoom != zoom) {
            oldZoom = zoom;
            [wmsTileLayer clearTileCache];
        }
        
    } else {
        
        button1.hidden = YES;
        button2.hidden = YES;
        button3.hidden = YES;
        button4.hidden = YES;
        button5.hidden = YES;
        _tileLayer.map = nil;
    }
}

- (void)addStairsMarker:(NSString *)lati :(NSString *)longi :(CGSize)newSize :(NSString *)imageName{
    
    UIImage *img = [UIImage imageNamed:imageName];
    img = [self imageWithImage:img scaledToSize:newSize];
    
    // Add a custom 'stairs' marker
    GMSMarker *stairsMarker = [[GMSMarker alloc] init];
    stairsMarker.title = @"Stairs";
    stairsMarker.icon = img;
    stairsMarker.position = CLLocationCoordinate2DMake([lati doubleValue], [longi doubleValue]);
    stairsMarker.map = mapView;
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


- (void)tileUpdate {
    
    wmsTileLayer.tileLevel = tileLevel;
    wmsTileLayer.map = mapView;
}

- (NSMutableDictionary *)cameraPositionCheck:(GMSCameraPosition*)position :(float)zoom {
    
    NSMutableDictionary *tileResponse = [NSMutableDictionary dictionary];
    
    if (position.target.latitude  > MunFreiheit.min_lat && position.target.latitude < MunFreiheit.max_lat &&
        position.target.longitude > MunFreiheit.min_lng && position.target.longitude < MunFreiheit.max_lng &&
        zoom > 16) {
        [tileResponse setObject:@"3" forKey:@"floorCount"];
        [tileResponse setObject:@"0,-1,-2" forKey:@"floorNames"];
        [tileResponse setObject:@"mun-frei" forKey:@"location"];
        [self tileUpdate];
        
    } else if (position.target.latitude  > MI_Building.min_lat && position.target.latitude < MI_Building.max_lat &&
               position.target.longitude > MI_Building.min_lng && position.target.longitude < MI_Building.max_lng &&
               zoom > 16) {
        [tileResponse setObject:@"4" forKey:@"floorCount"];
        [tileResponse setObject:@"3,2,1,0" forKey:@"floorNames"];
        [tileResponse setObject:@"mi" forKey:@"location"];
        [self tileUpdate];
    } else if (position.target.latitude  > MW_Building.min_lat && position.target.latitude < MW_Building.max_lat &&
               position.target.longitude > MW_Building.min_lng && position.target.longitude < MW_Building.max_lng &&
               zoom > 16) {
        [tileResponse setObject:@"4" forKey:@"floorCount"];
        [tileResponse setObject:@"3,2,1,0" forKey:@"floorNames"];
        [tileResponse setObject:@"mw" forKey:@"location"];
        [self tileUpdate];
    } else if (position.target.latitude  > MC_Building.min_lat && position.target.latitude < MC_Building.max_lat &&
               position.target.longitude > MC_Building.min_lng && position.target.longitude < MC_Building.max_lng &&
               zoom > 16) {
        [tileResponse setObject:@"5" forKey:@"floorCount"];
        [tileResponse setObject:@"4,3,2,1,0" forKey:@"floorNames"];
        [tileResponse setObject:@"mc" forKey:@"location"];
        [self tileUpdate];
    }
    
    return tileResponse;
}

- (IBAction)button1Action:(id)sender {
    NSLog(@"button 0 pressed");
    [mapView clear];//Clear table for new tile layer
    tileLevel = [NSString stringWithFormat:@"4"];//assign the level
    [self changeColorOfSelectedFloor:button1];//select the button
    [self tileUpdate];
    [self polylineArrayCheck];
    [self textFieldShouldReturn:topTextField : NO];
    //[self drawRouteFromDetail:receivedNotification];
}

- (IBAction)button2Action:(id)sender {
    NSLog(@"button 1 pressed");
    [mapView clear];
    tileLevel = [NSString stringWithFormat:@"3"];
    [self changeColorOfSelectedFloor:button2];
    [self tileUpdate];
    [self polylineArrayCheck];
    [self textFieldShouldReturn:topTextField : NO];
    //[self drawRouteFromDetail:receivedNotification];
}

- (IBAction)button3Action:(id)sender {
    NSLog(@"button 2 pressed");
    [mapView clear];
    tileLevel = [NSString stringWithFormat:@"2"];
    [self changeColorOfSelectedFloor:button3];
    [self tileUpdate];
    [self polylineArrayCheck];
    [self textFieldShouldReturn:topTextField : NO];
    //[self drawRouteFromDetail:receivedNotification];
}

- (IBAction)button4Action:(id)sender {
    NSLog(@"button 3 pressed");
    [mapView clear];
    tileLevel = [NSString stringWithFormat:@"1"];
    [self changeColorOfSelectedFloor:button4];
    [self tileUpdate];
    [self polylineArrayCheck];
    [self textFieldShouldReturn:topTextField : NO];
    //[self drawRouteFromDetail:receivedNotification];
}

- (IBAction)button5Action:(id)sender {
    NSLog(@"button 3 pressed");
    [mapView clear];
    tileLevel = [NSString stringWithFormat:@"0"];
    [self changeColorOfSelectedFloor:button5];
    [self tileUpdate];
    [self polylineArrayCheck];
    [self textFieldShouldReturn:topTextField : NO];
    //[self drawRouteFromDetail:receivedNotification];
}

- (IBAction)textFieldButtonAction:(id)sender {
    //[topTextField becomeFirstResponder];
    
    [renderer clearAllMarkers]; //COMMENT-IN
    previousRouteLine.strokeColor = [UIColor clearColor];
    previousRouteLine.map = nil;
    previousRouteLine = nil;
    //change view
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MapSearchVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"MapSearchVC"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)changeColorOfSelectedFloor:(UIButton *)selectedButton {
    
    button1.backgroundColor = [UIColor whiteColor];
    button2.backgroundColor = [UIColor whiteColor];
    button3.backgroundColor = [UIColor whiteColor];
    button4.backgroundColor = [UIColor whiteColor];
    button5.backgroundColor = [UIColor whiteColor];
    
    selectedButton.backgroundColor = [UIColor colorWithRed:0.23 green:0.69 blue:0.52 alpha:1.0];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField :(BOOL)openOverlay {
    [textField resignFirstResponder];
    NSLog(@"text:%@",textField.text);
    destinationNameBottomString = textField.text;
    
    //find the entrance in here then set starting point and the details
    //send destination building_id and find the entrance of the building and receive it as a JSON
    
    NSDictionary *startingPointRelativeToDestination = [self findBuildingStartingPoint:receivedDestinationRoom];
    
    if ([[startingPointRelativeToDestination valueForKey:@"building_id"] isEqualToString:@"mi"]) {
        [self setStartingPointMI];
    } else if ([[startingPointRelativeToDestination valueForKey:@"building_id"] isEqualToString:@"mw"]){
        [self setStartingPointMW];
    } else if ([[startingPointRelativeToDestination valueForKey:@"building_id"] isEqualToString:@"mc"]){
        [self setStartingPointMC];
    }
    
    [self drawRouting:textField.text :[startingPointRelativeToDestination valueForKey:@"building_id"]];
    
    NSLog(@"--dictionary-->%@",startingPointRelativeToDestination);
    
    destinationNameBottomString = [NSString stringWithFormat:@"%@, %@",textField.text, [startingPointRelativeToDestination valueForKey:@"building"]];
    destinationAddrBottomString = [NSString stringWithFormat:@"%@, %@ %@", [startingPointRelativeToDestination valueForKey:@"address"], [startingPointRelativeToDestination valueForKey:@"plz"], [startingPointRelativeToDestination valueForKey:@"city"]];
    
    if(textField.text.length > 0 && openOverlay){
        [self didTapFlyIn];
    }
    
    //setup the entrance based on the flag
    if(!updateStartingPoint) {
        startingPoint.latlng.latitute = [NSString stringWithFormat:@"%.6f",[[startingPointRelativeToDestination valueForKey:@"starting_latitude"] doubleValue]];
        startingPoint.latlng.longitude = [NSString stringWithFormat:@"%.6f",[[startingPointRelativeToDestination valueForKey:@"starting_longitude"] doubleValue]];
        startingPoint.identifier = F(@"%@", startingPoint.identifier);//F(@"%.6f,%.6f", [startingPoint.latlng.longitude doubleValue], [startingPoint.latlng.latitute doubleValue]);
    }
    
    NSLog(@"startingP->%@ - %@",startingPoint.latlng.latitute,startingPoint.latlng.longitude);

    [_overlay initWithNameAndAddr:destinationNameBottomString andAddr:destinationAddrBottomString];
    
    //add markers to start & end positions
    CLLocationCoordinate2D coor1 = CLLocationCoordinate2DMake([startingPoint.latlng.latitute doubleValue], [startingPoint.latlng.longitude doubleValue]);
    [self createMarker:coor1 :startingPoint.identifier];
    
    LatLngWithTags *destinationLatLng = [locUtil findDestinationFromId:textField.text];
    CLLocationCoordinate2D coor2 = CLLocationCoordinate2DMake([destinationLatLng.latlng.latitute doubleValue], [destinationLatLng.latlng.longitude doubleValue]);
    [self createMarker:coor2 :destinationLatLng.identifier];
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

- (NSDictionary *)findBuildingStartingPoint:(Room *)destination {
    
    NSDictionary *response;
    
    NSDictionary *searchedPath;
    if ([destination.building_id isEqualToString:@"mi"]) {
        searchedPath = [geoJsonArray objectAtIndex:0];
    } else if ([destination.building_id isEqualToString:@"mw"]) {
        searchedPath = [geoJsonArray objectAtIndex:1];
    } else if ([destination.building_id isEqualToString:@"mc"]) {
        searchedPath = [geoJsonArray objectAtIndex:2];
    }
    
    for (NSDictionary *dict in [searchedPath valueForKey:@"features"]){
        if ([[dict valueForKey:@"properties"] valueForKey:@"id"] &&
            [[[dict valueForKey:@"properties"] valueForKey:@"building_id"] isEqualToString:destination.building_id] &&
            [[[dict valueForKey:@"properties"] valueForKey:@"id"] isEqualToString:@"entrance"]){
            
            response = [NSDictionary dictionaryWithObjectsAndKeys:
                        [[dict valueForKey:@"properties"] valueForKey:@"level"], @"level",
                        [[dict valueForKey:@"properties"] valueForKey:@"id"], @"id",
                        [[dict valueForKey:@"properties"] valueForKey:@"building_id"], @"building_id",
                        [[dict valueForKey:@"properties"] valueForKey:@"building"], @"building",
                        [[dict valueForKey:@"properties"] valueForKey:@"city"], @"city",
                        [[dict valueForKey:@"properties"] valueForKey:@"plz"], @"plz",
                        [[dict valueForKey:@"properties"] valueForKey:@"address"], @"address",
                        F(@"%@", [[[dict valueForKey:@"geometry"] valueForKey:@"coordinates"] objectAtIndex:0]), @"starting_longitude",
                        F(@"%@", [[[dict valueForKey:@"geometry"] valueForKey:@"coordinates"] objectAtIndex:1]), @"starting_latitude",
                        nil];
        }
    }
    
    return response;
}

- (void)drawRouting:(NSString *)destination :(NSString *)buildingId{
    
    NSLog(@"startingP->%@ - %@",startingPoint.latlng.latitute,startingPoint.latlng.longitude);
    NSDictionary *searchedPath;

    if ([buildingId isEqualToString:@"mi"]) {
        searchedPath = [geoJsonArray objectAtIndex:0];
    } else if ([buildingId isEqualToString:@"mw"]) {
        searchedPath = [geoJsonArray objectAtIndex:1];
    } else if ([buildingId isEqualToString:@"mc"]) {
        searchedPath = [geoJsonArray objectAtIndex:2];
    }
    
    geoJSONDijkstra = [locUtil processIndoorPathFeatures:searchedPath :geoJSONDijkstra];
    mapView = [locUtil handleRouteRequest2:mapView :drawStyle :geoJSONDijkstra :startingPoint :destination];
    [renderer clearAllMarkers]; //COMMENT-IN
    
    NSLog(@"startingP->%@ - %@",startingPoint.latlng.latitute,startingPoint.latlng.longitude);

}

- (void)changeMapRoute {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MapRouteVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"MapRouteVC"];
    vc.sourceString = @"Entrance of the building";
    vc.destinationString = topTextField.text;

    NSLog(@"%@",startingPoint);
    NSLog(@"%@",destiPoint);
    NSLog(@"**");
//    Room *sp = [[Room alloc] init];
//    sp.building_id
//
    //vc.source
    //vc.destination
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)mapTypeSelection {
    
    //temporary use for clearing the map from markers and lines
    [mapView clear];
    
//    NSLog(@"%lu",(unsigned long)mapView.mapType);
//
//    if(mapView.mapType == kGMSTypeNormal) {
//        mapView.mapType = kGMSTypeSatellite;
//    } else if (mapView.mapType == kGMSTypeSatellite) {
//        mapView.mapType = kGMSTypeHybrid;
//    } else if (mapView.mapType == kGMSTypeHybrid) {
//        mapView.mapType = kGMSTypeTerrain;
//    } else if (mapView.mapType == kGMSTypeTerrain) {
//        mapView.mapType = kGMSTypeNormal;
//    }
}

- (void)moveOverlayChange {
    UIEdgeInsets padding = mapView.padding;
    [UIView animateWithDuration:0.5 animations:^{
        if (padding.bottom == 0.0f) {
            [self overlayAndButtonsGoUp];
        } else {
            [self overlayAndButtonsGoDown];
        }
    } completion:^(BOOL finished) {
        if (_overlay.frame.size.height == 0) {
            _overlay.hidden = YES;
        }
    }];
}

- (void)moveOverlayDown {
    UIEdgeInsets padding = mapView.padding;
    [UIView animateWithDuration:0.5 animations:^{
        if (padding.bottom != 0.0f) {
            [self overlayAndButtonsGoDown];
        }
    } completion:^(BOOL finished) {
        _overlay.hidden = YES;
    }];
}

- (void)overlayAndButtonsGoUp {
    CGSize size = self.view.bounds.size;
    _overlay.frame = CGRectMake(0, size.height - kOverlayHeight, size.width, kOverlayHeight);
    mapView.padding = UIEdgeInsetsMake(0, 0, kOverlayHeight, 0);
    _overlay.hidden = NO;
    // move the floor numbers up
    button5.frame = CGRectMake(button5.frame.origin.x, button5.frame.origin.y - 110, button5.frame.size.width, button5.frame.size.height);
    button4.frame = CGRectMake(button4.frame.origin.x, button4.frame.origin.y - 110, button4.frame.size.width, button4.frame.size.height);
    button3.frame = CGRectMake(button3.frame.origin.x, button3.frame.origin.y - 110, button3.frame.size.width, button3.frame.size.height);
    button2.frame = CGRectMake(button2.frame.origin.x, button2.frame.origin.y - 110, button2.frame.size.width, button2.frame.size.height);
    button1.frame = CGRectMake(button1.frame.origin.x, button1.frame.origin.y - 110, button1.frame.size.width, button1.frame.size.height);
}


- (void)overlayAndButtonsGoDown {
    CGSize size = self.view.bounds.size;
    _overlay.frame = CGRectMake(0, mapView.bounds.size.height, size.width, 0);
    mapView.padding = UIEdgeInsetsZero;
    button5.frame = CGRectMake(button5.frame.origin.x, button5.frame.origin.y + 110, button5.frame.size.width, button5.frame.size.height);
    button4.frame = CGRectMake(button4.frame.origin.x, button4.frame.origin.y + 110, button4.frame.size.width, button4.frame.size.height);
    button3.frame = CGRectMake(button3.frame.origin.x, button3.frame.origin.y + 110, button3.frame.size.width, button3.frame.size.height);
    button2.frame = CGRectMake(button2.frame.origin.x, button2.frame.origin.y + 110, button2.frame.size.width, button2.frame.size.height);
    button1.frame = CGRectMake(button1.frame.origin.x, button1.frame.origin.y + 110, button1.frame.size.width, button1.frame.size.height);
}


- (void)clearTextField {
    
    topTextField.text = @"";
    [self moveOverlayDown];
    cancelButton.hidden = YES;
    //clean the route
    [mapView clear];
    wmsTileLayer.tileLevel = tileLevel;
    wmsTileLayer.map = mapView;
}


- (void)didTapFlyIn {
    UIEdgeInsets padding = mapView.padding;
    
    [UIView animateWithDuration:0.5 animations:^{
        CGSize size = self.view.bounds.size;
        if (padding.bottom == 0.0f) {
            _overlay.frame = CGRectMake(0, size.height - kOverlayHeight, size.width, kOverlayHeight);
            mapView.padding = UIEdgeInsetsMake(0, 0, kOverlayHeight, 0);
            _overlay.hidden = NO;
            // move the floor numbers up
            button5.frame = CGRectMake(button5.frame.origin.x, button5.frame.origin.y - 110, button5.frame.size.width, button5.frame.size.height);
            button4.frame = CGRectMake(button4.frame.origin.x, button4.frame.origin.y - 110, button4.frame.size.width, button4.frame.size.height);
            button3.frame = CGRectMake(button3.frame.origin.x, button3.frame.origin.y - 110, button3.frame.size.width, button3.frame.size.height);
            button2.frame = CGRectMake(button2.frame.origin.x, button2.frame.origin.y - 110, button2.frame.size.width, button2.frame.size.height);
            button1.frame = CGRectMake(button1.frame.origin.x, button1.frame.origin.y - 110, button1.frame.size.width, button1.frame.size.height);
        } else {
            _overlay.frame = CGRectMake(0, mapView.bounds.size.height, size.width, 0);
            mapView.padding = UIEdgeInsetsZero;
            // move the floor numbers down
            button5.frame = CGRectMake(button5.frame.origin.x, button5.frame.origin.y + 110, button5.frame.size.width, button5.frame.size.height);
            button4.frame = CGRectMake(button4.frame.origin.x, button4.frame.origin.y + 110, button4.frame.size.width, button4.frame.size.height);
            button3.frame = CGRectMake(button3.frame.origin.x, button3.frame.origin.y + 110, button3.frame.size.width, button3.frame.size.height);
            button2.frame = CGRectMake(button2.frame.origin.x, button2.frame.origin.y + 110, button2.frame.size.width, button2.frame.size.height);
            button1.frame = CGRectMake(button1.frame.origin.x, button1.frame.origin.y + 110, button1.frame.size.width, button1.frame.size.height);
        }
    } completion:^(BOOL finished) {
        if (_overlay.frame.size.height == 0) {
            //_overlay.hidden = YES;
        }
    }];
}

- (void)drawRouteFromDetail:(NSNotification *) notification {

    if([notification.name isEqualToString:@"drawRouteFromDetail"]) {
        receivedNotification = notification;
        
        for(GMSMarker *m in markerList){
            m.map = nil;
        }
        previousRouteLine.strokeColor = [UIColor clearColor];
        previousRouteLine.map = nil;
        previousRouteLine = nil;
        //receive NSnotification
        NSDictionary* userInfo = notification.userInfo;
        NSString *sourceString = (NSString*)userInfo[@"sourceAddress"];
        NSString *destinationString = (NSString*)userInfo[@"destinationAddress"];
        NSLog(@"Source: %@\nDestination:%@", sourceString, destinationString);
        LatLngWithTags *sourceLatLng = [locUtil findDestinationFromId:sourceString];
        LatLngWithTags *destLatLng = [locUtil findDestinationFromId:destinationString];
        
        updateStartingPoint = YES;

        // change starting point values
        startingPoint = [locUtil setupStartingPoint:F(@"%@",sourceLatLng.latlng.latitute)
                                                   :F(@"%@",sourceLatLng.latlng.longitude)
                                                   :F(@"%@",sourceLatLng.level)
                                                   :F(@"%@",sourceLatLng.identifier)];
        //[self drawRouting:destinationString];
        
        //add markers to start & end positions
        CLLocationCoordinate2D coor1 = CLLocationCoordinate2DMake([sourceLatLng.latlng.latitute doubleValue], [sourceLatLng.latlng.longitude doubleValue]);
        [self createMarker:coor1 :sourceLatLng.identifier];
        
        CLLocationCoordinate2D coor2 = CLLocationCoordinate2DMake([destLatLng.latlng.latitute doubleValue], [destLatLng.latlng.longitude doubleValue]);
        [self createMarker:coor2 :destLatLng.identifier];
        
        if (receivedNotification != nil) {
            [self zoomIntoDestination:[markerList lastObject]];
        }
    }
}

- (void)polylineArrayCheck {
    
    for (int i = 0;i< receivedPolylineArray.count;i++) {
        if ([tileLevel isEqualToString:[NSString stringWithFormat:@"%d",i]]) {
            [receivedPolylineArray[i] setStrokeColor:[UIColor redColor]];
        } else{
            [receivedPolylineArray[i] setStrokeColor:[UIColor grayColor]];
        }
    }
}

- (void)zoomIntoDestination:(GMSMarker *)marker {
    
    [CATransaction begin];
    [CATransaction setAnimationDuration:3.f];  // 3 second animation
    
    GMSCameraPosition *camera = [[GMSCameraPosition alloc] initWithTarget:marker.position zoom:18 bearing:0 viewingAngle:0];
    [mapView animateToCameraPosition:camera];
    [CATransaction commit];
}

//base url address for tile server 10.19.1.52
//münchner freiheit 0,-1,-2
//new LatLng(48.162697, 11.587385), new LatLng(48.160468, 11.584937) //max,max,min,min
//String s = String.format("http://" + ipAddress + "/hot" + level + "/%d/%d/%d.png",zoom, x, y);

@end
