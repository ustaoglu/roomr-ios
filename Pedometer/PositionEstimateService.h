//
//  PositionEstimateService.h
//  Pedometer
//
//  Created by Efdal Ustaoglu on 15.02.18.
//  Copyright © 2018 Navindo GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Macros.h"
@protocol PositionEstimateServiceProtocol <NSObject>
-(void)outputReceived:(NSArray *)userInfo;
@end

@interface PositionEstimateService : NSObject
@property (nonatomic,weak) id <PositionEstimateServiceProtocol> delegate;
- (void)estimatePosition:(NSMutableDictionary *)positionDict;
@end
