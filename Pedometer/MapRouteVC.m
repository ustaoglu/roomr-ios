//
//  MapRouteVC.m
//  Pedometer
//
//  Created by Efdal Ustaoglu on 09.04.18.
//  Copyright © 2018 Navindo GmbH. All rights reserved.
//

#import "MapRouteVC.h"

@interface MapRouteVC ()

@end

@implementation MapRouteVC
@synthesize rightChangeView;
@synthesize source, destination;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    rightChangeView.layer.cornerRadius = rightChangeView.frame.size.height / 2;
    
    self.leftSourceSmallView.layer.cornerRadius = self.leftSourceSmallView.frame.size.height / 2;
    self.leftDestinationSmallView.layer.cornerRadius = self.leftDestinationSmallView.frame.size.height / 2;
    self.sourceTextField.text = self.sourceString;
    self.destinationTextField.text = self.destinationString;
}

- (Room *)findPositionById: (NSString *)roomId {
    
    Room *roomToBeFound;
    
    
    return roomToBeFound;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)rightChangeButtonAction:(id)sender {
    NSString *tmpStr = self.sourceTextField.text;
    self.sourceTextField.text = self.destinationTextField.text;
    self.destinationTextField.text = tmpStr;
}

- (IBAction)goBack:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)searchButtonAction:(id)sender {
//  send new positions to prev. view and draw a line
    NSDictionary* userInfo = @{@"sourceAddress": self.sourceTextField.text,
                               @"destinationAddress": self.destinationTextField.text
                               };
    
    NSLog(@"source:%@",source);
    NSLog(@"destination:%@",destination);
    
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"drawRouteFromDetail" object:self userInfo:userInfo];
    [self.navigationController popToRootViewControllerAnimated:YES];    
}

- (IBAction)sourceButtonAction:(id)sender {
    
    //change view to list view
    //after selection send the room name
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MapSearchVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"MapSearchVC"];
    vc.comesFromRouteSelection = TRUE;
    vc.delegate = self;
    vc.option = 1;
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)destinationButtonAction:(id)sender {
    
    //change view to list view
    //after selection send the room name
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MapSearchVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"MapSearchVC"];
    vc.comesFromRouteSelection = TRUE;
    vc.delegate = self;
    vc.option = 2;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didDestionationSelection:(Room *)result :(int)option {
    
    NSLog(@"received result%@ - int:%d", result.room_id, option);
    //assign the values    
    if(option == 1) {
        source = [[Room alloc] init];
        source.level = F(@"%@", result.level);
        source.room_id = F(@"%@", result.room_id);
        source.building_id = F(@"%@", result.building_id);
        self.sourceString = result.room_id;
        self.sourceTextField.text = self.sourceString;
    } else if(option == 2) {
        destination = [[Room alloc] init];
        destination.level = F(@"%@", result.level);
        destination.room_id = F(@"%@", result.room_id);
        destination.building_id = F(@"%@", result.building_id);
        self.destinationString = result.room_id;
        self.destinationTextField.text = self.destinationString;
    }
}


@end
