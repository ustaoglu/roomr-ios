//
//  MapSearchVC.h
//  Pedometer
//
//  Created by Efdal Ustaoglu on 12.04.18.
//  Copyright © 2018 Navindo GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Macros.h"
#import "Room.h"

@protocol MapSearchVCDelegate <NSObject>
- (void)didDestionationSelection:(Room *)result :(int)option;
@end

@interface MapSearchVC : UIViewController<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *myTextField;
- (IBAction)backButtonAction:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *myTableView;

@property (nonatomic, retain) id <MapSearchVCDelegate> delegate;
@property BOOL  comesFromRouteSelection;
@property int   option;

@end
