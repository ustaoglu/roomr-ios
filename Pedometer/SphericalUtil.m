//
//  SphericalUtil.m
//
//  Created by Cameron Carmichael Alonso on 01/06/2014.
//  Copyright (c) 2014 Cameron Carmichael Alonso. All rights reserved.
//

#import "SphericalUtil.h"

@implementation SphericalUtil
#define EARTH_RADIUS 6371
#define degreesToRadians( degrees ) ( ( degrees ) / 180.0 * M_PI )
#define radiansToDegrees( radians ) ( ( radians ) * ( 180.0 / M_PI ) )

/**
 * Returns the LatLng resulting from moving a distance from an origin
 * in the specified heading (expressed in degrees clockwise from north).
 * @param lat     The latitude from which to start.
 * @param lng     The longitude from which to start.
 * @param distance The distance to travel.
 * @param heading  The heading in degrees clockwise from north.
 */
-(void)computeOffsetWithLatitude:(double)lat longitude:(double)lng distance:(double)distance heading:(double)heading withCompletion:(SphericalUtilCompletionBlock)callback {
    
    distance /= EARTH_RADIUS;
    heading = degreesToRadians(heading);
    double fromLat = degreesToRadians(lat);
    double fromLng = degreesToRadians(lng);
    double cosDistance = cos(distance);
    double sinDistance = sin(distance);
    double sinFromLat = sin(fromLat);
    double cosFromLat = cos(fromLat);
    double sinLat = cosDistance * sinFromLat + sinDistance * cosFromLat * cos(heading);
    double dLng = atan2(
                        sinDistance * cosFromLat * sin(heading),
                        cosDistance - sinFromLat * sinLat);
  
    double asinLat = asin(sinLat);
    double fromLngPlusdLng = fromLng + dLng;
    
    double latToReturn = radiansToDegrees(asinLat);
    double lngToReturn = radiansToDegrees(fromLngPlusdLng);
    
    callback (latToReturn, lngToReturn);
}

@end
