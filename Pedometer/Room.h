//
//  Room.h
//  Pedometer
//
//  Created by Efdal Ustaoglu on 27.06.18.
//  Copyright © 2018 Pinkstone Pictures LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Room : NSObject

@property (nonatomic,strong) NSString *room_id;
@property (nonatomic,strong) NSString *building_id;
@property (nonatomic,strong) NSString *level;

@end
