//
//  ImageHandler.m
//  Pedometer
//
//  Created by HiWi on 04.06.18.
//  Copyright © 2018 Navindo GmbH. All rights reserved.
//

#import "ImageHandler.h"

@implementation ImageHandler

- (void)saveImage: (UIImage*)image :(NSString *)x :(NSString *)y :(NSString *)level :(NSString *)zoom
{
    if (image != nil)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                             NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithString: @"x"] ];
        NSData* data = UIImagePNGRepresentation(image);
        [data writeToFile:path atomically:YES];
    }
}

- (UIImage*)loadImage :(NSString *)x :(NSString *)y :(NSString *)level :(NSString *)zoom
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:
                      [NSString stringWithString: @"test.png"] ];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    return image;
}

@end
