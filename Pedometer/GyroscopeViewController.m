//
//  GyroscopeViewController.m
//  Pedometer
//
//  Created by Efdal Ustaoglu on 06.12.17.
//  Copyright © 2018 Ariadne TUM. All rights reserved.
//

#import "GyroscopeViewController.h"

@interface GyroscopeViewController ()

@end

@implementation GyroscopeViewController

static NSString *const AWSSampleDynamoDBTableName = @"Music";

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    currentMaxAccelX = 0;
    currentMaxAccelY = 0;
    currentMaxAccelZ = 0;
    
    currentMaxRotX = 0;
    currentMaxRotY = 0;
    currentMaxRotZ = 0;
    
    self.motionManager = [[CMMotionManager alloc] init];
    self.motionManager.accelerometerUpdateInterval = .2;
    self.motionManager.gyroUpdateInterval = .2;

//    self.ref = [[FIRDatabase database] reference];
//    [[[_ref child:@"users"] child:@"3"] setValue:@{@"username": @"nikost"}];
    
    FIRDatabaseReference *_refHandle =  [[FIRDatabase database] reference];
//    NSString *_postRef = [_refHandle child:@"users"];
//    _refHandle =
//    NSLog(@"postRef:%@",_postRef);
    
//    [_refHandle observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
//        NSDictionary *postDict = snapshot.value;
//        NSLog(@"Dict:%@",postDict);
//    }];
    
//    [self.motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue currentQueue]
//                                             withHandler:^(CMAccelerometerData *accelerometerData, NSError *error) {
//                                                 [self outputAccelertionData:accelerometerData.acceleration];
//                                                 if(error){
//
//                                                     NSLog(@"%@", error);
//                                                 }
//                                             }];

    [self.motionManager startGyroUpdatesToQueue:[NSOperationQueue currentQueue]
                                    withHandler:^(CMGyroData *gyroData, NSError *error) {
                                        [self outputRotationData:gyroData.rotationRate];
                                    }];
}

- (void)generateTestData {
//    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
//    
//    AWSDynamoDBObjectMapper *dynamoDBObjectMapper = [AWSDynamoDBObjectMapper defaultDynamoDBObjectMapper];
//    
//    NSMutableArray *tasks = [NSMutableArray array];
//    NSArray *gameTitleArray = @[@"Galaxy Invaders",@"Meteor Blasters", @"Starship X", @"Alien Adventure",@"Attack Ships"];
//    for (int32_t i = 0; i < 25; i++) {
//        for (int32_t j = 0 ; j < 2; j++) {
//            DDBTableRow *tableRow = [DDBTableRow new];
//            tableRow.UserId = [NSString stringWithFormat:@"%d",i];
//            
//            tableRow.GameTitle = j==0?gameTitleArray[arc4random_uniform((u_int32_t)gameTitleArray.count)]:@"Comet Quest";
//            tableRow.TopScore = [NSNumber numberWithInt:arc4random_uniform(3000)];
//            tableRow.Wins = [NSNumber numberWithInteger:arc4random_uniform(100)];
//            tableRow.Losses = [NSNumber numberWithInteger:arc4random_uniform(100)];
//            
//            //Those two properties won't be saved to DynamoDB since it has been defined in ignoredAttributes
//            tableRow.internalName =[NSString stringWithFormat:@"internal attributes(should not be saved to dynamoDB)"];
//            tableRow.internalState = [NSNumber numberWithInt:i];
//            
//            [tasks addObject:[dynamoDBObjectMapper save:tableRow]];
//        }
//    }
//    
//    [[AWSTask taskForCompletionOfAllTasks:tasks]
//     continueWithExecutor:[AWSExecutor mainThreadExecutor] withBlock:^id(AWSTask *task) {
//         if (task.error) {
//             NSLog(@"Error: [%@]", task.error);
//         }
//         
//         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
//         
//         return [self refreshList:YES];
//     }];
}

-(void)outputAccelertionData:(CMAcceleration)acceleration {
    
    self.accX.text = [NSString stringWithFormat:@" %.2fg",acceleration.x];
    if(fabs(acceleration.x) > fabs(currentMaxAccelX))
    {
        currentMaxAccelX = acceleration.x;
    }
    self.accY.text = [NSString stringWithFormat:@" %.2fg",acceleration.y];
    if(fabs(acceleration.y) > fabs(currentMaxAccelY))
    {
        currentMaxAccelY = acceleration.y;
    }
    self.accZ.text = [NSString stringWithFormat:@" %.2fg",acceleration.z];
    if(fabs(acceleration.z) > fabs(currentMaxAccelZ))
    {
        currentMaxAccelZ = acceleration.z;
    }
    
    self.maxAccX.text = [NSString stringWithFormat:@" %.2f",currentMaxAccelX];
    self.maxAccY.text = [NSString stringWithFormat:@" %.2f",currentMaxAccelY];
    self.maxAccZ.text = [NSString stringWithFormat:@" %.2f",currentMaxAccelZ];
}

-(void)outputRotationData:(CMRotationRate)rotation{
    
    self.rotX.text = [NSString stringWithFormat:@" %.2fd/s", RADIANS_TO_DEGREES(rotation.x)];
    if(fabs(rotation.x)> fabs(currentMaxRotX))
    {
        currentMaxRotX = rotation.x;
    }
    self.rotY.text = [NSString stringWithFormat:@" %.2fd/s", RADIANS_TO_DEGREES(rotation.y)];
    if(fabs(rotation.y) > fabs(currentMaxRotY))
    {
        currentMaxRotY = rotation.y;
    }
    self.rotZ.text = [NSString stringWithFormat:@" %.2fd/s",RADIANS_TO_DEGREES(rotation.z)];
    if(RADIANS_TO_DEGREES(rotation.z) > currentMaxRotZ)
    {
        currentMaxRotZ = RADIANS_TO_DEGREES(rotation.z);
    }
    
    self.maxRotX.text = [NSString stringWithFormat:@" %.2f",currentMaxRotX];
    self.maxRotY.text = [NSString stringWithFormat:@" %.2f",currentMaxRotY];
    self.maxRotZ.text = [NSString stringWithFormat:@" %.2f",currentMaxRotZ];
}

- (IBAction)resetMaxValues:(id)sender {
    
    currentMaxAccelX = 0;
    currentMaxAccelY = 0;
    currentMaxAccelZ = 0;
    
    currentMaxRotX = 0;
    currentMaxRotY = 0;
    currentMaxRotZ = 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
