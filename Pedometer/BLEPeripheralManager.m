////
////  BLEPeripheralManager.m
////  BLEScanner
////
////  Created by Jason George on 12/23/15.
////  Copyright © 2018 Jason George. All rights reserved.
////
//
//#import "BLEPeripheralManager.h"
//#import "BLEPeripheral.h"
//
//@implementation BLEPeripheralManager
//{
//    CBCentralManager *_centralManager;
//    NSMutableArray *_peripheralBuffer;
//    NSTimer *_tableCleanTimer;
//}
//
//#pragma mark - Lifecycle Management
//
//- (instancetype)init
//{
//    self = [super init];
//    if (self) {
//        _peripheralBuffer = [NSMutableArray new];
//        _state = BLEPeripheralManagerStateOffline;
//        _centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
//        _tableCleanTimer = [NSTimer scheduledTimerWithTimeInterval:5.0
//                                                      target:self
//                                                    selector:@selector(clearListEvery5Seconds)
//                                                    userInfo:nil
//                                                     repeats:YES];
//    }
//    return self;
//}
//
//+ (instancetype)sharedInstance
//{
//    NSAssert(self == [BLEPeripheralManager class], @"BLEPeripheralManager is not designed to be subclassed");
//    
//    static BLEPeripheralManager *sharedPeripheralManager;
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        sharedPeripheralManager = [BLEPeripheralManager new];
//    });
//    
//    return sharedPeripheralManager;
//}
//
//#pragma mark - Operations
//
//- (void)scanForPeripherals
//{
//    if (_state == BLEPeripheralManagerStateAvailable) {
//        _peripheralBuffer = [NSMutableArray new];
//        _state = BLEPeripheralManagerStateScanning;
//        NSDictionary * options = [NSDictionary dictionaryWithObjectsAndKeys:@YES, CBCentralManagerScanOptionAllowDuplicatesKey, nil];
//        [_centralManager scanForPeripheralsWithServices:nil options:options];
//    }
//}
//
//- (void)stopScan
//{
//    if (_state == BLEPeripheralManagerStateScanning) {
//        [_centralManager stopScan];
//        _state = BLEPeripheralManagerStateAvailable;
//    }
//}
//
//#pragma mark - CBCentralManagerDelegate Protocol
//
//- (void)centralManager:(CBCentralManager *)central
//  didConnectPeripheral:(CBPeripheral *)peripheral
//{
//    [peripheral discoverServices:nil];
////    NSLog(@"Connected to %@: %@",
////          [peripheral name],
////          [peripheral state] == CBPeripheralStateConnected ? @"YES" : @"NO");
//}
//
//- (void)centralManager:(CBCentralManager *)central
// didDiscoverPeripheral:(CBPeripheral *)peripheral
//     advertisementData:(NSDictionary<NSString *,id> *)advertisementData
//                  RSSI:(NSNumber *)RSSI
//{
//    BOOL isExist = NO;
//    for (BLEPeripheral *bleP in _peripheralBuffer) {
//        if ([[bleP name] isEqualToString:[peripheral name]] &&
//            [[[peripheral identifier] UUIDString] isEqualToString:bleP.uuid] /*&&
//            [bleP.name isEqualToString:@"Kontakt"]*/ && [RSSI intValue] < -20) {
//                
//                NSLog(@"ADVERST data, %@",advertisementData);
//                
//                bleP.rssi_value = [NSString stringWithFormat:@"%@",RSSI];
//                isExist = YES;
//                //NSLog(@"Update!");
//                break;
//        }
//    }
//    
//    if (!isExist) {
//        
//        BLEPeripheral *blePeripheral = [[BLEPeripheral alloc] initWithCBPeripheral:peripheral];
//        blePeripheral.rssi_value = [NSString stringWithFormat:@"%@",RSSI];
//        //if (![blePeripheral.name isEqualToString:@"<Unknown Device>"] && [blePeripheral.name isEqualToString:@"Kontakt"] ) {
//        if ([RSSI intValue] < -20 && ![blePeripheral.name isEqualToString:@"<Unknown Device>"]/*&& [blePeripheral.name isEqualToString:@"Kontakt"]*/) {
//            [peripheral setDelegate:blePeripheral];
//            [_peripheralBuffer addObject:blePeripheral];
////            NSDictionary * options = [NSDictionary dictionaryWithObjectsAndKeys:@YES, CBCentralManagerScanOptionAllowDuplicatesKey, nil];
////            [_centralManager connectPeripheral:peripheral options:options];
//        }
//    }
//    
////    NSLog(@"-->%@",[self createDictionaryFromBLE]);
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshTableView" object:self];
//    [self createDictionaryFromBLE];
////    [[NSNotificationCenter defaultCenter] postNotificationName:@"bleDictionary" object:self userInfo:[self createDictionaryFromBLE]];
//    
////    NSUInteger peripheralIndex = [_peripheralBuffer indexOfObjectPassingTest:^BOOL(id object, NSUInteger index, BOOL *stop) {
////        if ([object isKindOfClass:[BLEPeripheral class]]) {
////
////            BLEPeripheral *blePeripheral = (BLEPeripheral *)object;
////            if ([blePeripheral isEqualToCBPeripheral:peripheral] /* && [RSSI intValue] != [[blePeripheral rssi_value] intValue] */) {
////                NSLog(@"Duplicate: %@",[blePeripheral name]);
////                blePeripheral.rssi_value = [NSString stringWithFormat:@"%@",RSSI];
////                _peripheralBuffer[index] = blePeripheral;
////                *stop = YES;
////                return YES;
////            }
////        }
////        return NO;
////    }];
//   
////    BOOL isExist = NO;
////    for (BLEPeripheral *bleP in _peripheralBuffer){
////        if ([[bleP name] isEqualToString:[peripheral name]]) {
////            isExist = YES;
////            break;
////        }
////    }
//    
////    if (peripheralIndex == NSNotFound) {
////        NSLog(@"Discovered: %@",[peripheral name]);
////        BLEPeripheral *blePeripheral = [[BLEPeripheral alloc] initWithCBPeripheral:peripheral];
////        blePeripheral.rssi_value = [NSString stringWithFormat:@"%@",RSSI];
////        [peripheral setDelegate:blePeripheral];
////        [_peripheralBuffer addObject:blePeripheral];
////        NSDictionary * options = [NSDictionary dictionaryWithObjectsAndKeys:@YES, CBCentralManagerScanOptionAllowDuplicatesKey, nil];
////        [_centralManager connectPeripheral:peripheral options:options];
////    }
//}
//
//- (void)createDictionaryFromBLE {
//    NSMutableDictionary *bleDict = [[NSMutableDictionary alloc] init];
//    for (BLEPeripheral *blep in _peripheralBuffer) {
////        NSLog(@"->%@",blep.name);
//        if (![blep.name isEqualToString:@"<Unknown Device>"] && /*[blep.name isEqualToString:@"Kontakt"] &&*/ blep.uuid != nil && blep.rssi_value != nil && [blep.rssi_value intValue] < -20) {
//            NSMutableDictionary *temp = [[NSMutableDictionary alloc] init];
//            [temp setValue:F(@"%@",blep.name) forKey:@"BLT-name"];
//            [temp setValue:F(@"%@",blep.uuid) forKey:@"BLT-uid"];
//            [temp setValue:F(@"%@",blep.rssi_value) forKey:@"BLT-strength"];
//            [bleDict setValue:temp forKey:blep.uuid];
//        }
//    }
//    if (bleDict.count > 0) {
//
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"bleDictionary" object:nil userInfo:[NSDictionary dictionaryWithDictionary:bleDict]];
//    }
//    //return [NSDictionary dictionaryWithDictionary:bleDict];
//}
//
//
//- (void)centralManagerDidUpdateState:(CBCentralManager *)central
//{
//    if (_state == BLEPeripheralManagerStateScanning) {
////        [_centralManager stopScan];
//    }
//    
//    switch ([central state]) {
//        case CBCentralManagerStatePoweredOff:
//            NSLog(@"CoreBluetooth BLE hardware is powered off");
//            _state = BLEPeripheralManagerStateOffline;
//            break;
//            
//        case CBCentralManagerStatePoweredOn:
//            NSLog(@"CoreBluetooth BLE hardware is powered on and ready");
//            _state = BLEPeripheralManagerStateAvailable;
//            break;
//            
//        case CBCentralManagerStateUnauthorized:
//            NSLog(@"CoreBluetooth BLE state is unauthorized");
//            _state = BLEPeripheralManagerStateOffline;
//            break;
//            
//        case CBCentralManagerStateUnsupported:
//            NSLog(@"CoreBluetooth BLE hardware is unsupported on this platform");
//            _state = BLEPeripheralManagerStateOffline;
//            break;
//            
//        case CBCentralManagerStateUnknown:
//        default:
//            NSLog(@"CoreBluetooth BLE state is unknown");
//            _state = BLEPeripheralManagerStateOffline;
//            break;
//    }
//}
//
//- (void)clearListEvery5Seconds {
//    _peripheralBuffer = [NSMutableArray new];
//}
//
//#pragma mark - Property Overrides
//
//- (NSArray *)peripherals
//{
//    return [_peripheralBuffer copy];
//}
//
//@end
