//
//  MapRouteVC.h
//  Pedometer
//
//  Created by Efdal Ustaoglu on 09.04.18.
//  Copyright © 2018 Navindo GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapSearchVC.h"
#import "Macros.h"

@interface MapRouteVC : UIViewController<MapSearchVCDelegate>

@property (strong, nonatomic) IBOutlet UITextField *sourceTextField;
@property (strong, nonatomic) IBOutlet UITextField *destinationTextField;

@property (strong, nonatomic) IBOutlet UIView *rightChangeView;

@property (strong, nonatomic) IBOutlet UIView *leftSourceSmallView;
@property (strong, nonatomic) IBOutlet UIView *leftDestinationSmallView;

@property (strong, nonatomic) IBOutlet UIButton *rightChangeButton;
- (IBAction)rightChangeButtonAction:(id)sender;
- (IBAction)goBack:(id)sender;

@property (strong, nonatomic) NSString *sourceString;
@property (strong, nonatomic) NSString *destinationString;
@property (strong, nonatomic) IBOutlet UIButton *searchButton;
- (IBAction)searchButtonAction:(id)sender;

- (IBAction)sourceButtonAction:(id)sender;

- (IBAction)destinationButtonAction:(id)sender;

@property (strong, nonatomic) Room *source;
@property (strong, nonatomic) Room *destination;

@end
