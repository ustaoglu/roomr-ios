//
//  SphericalUtil.h
//
//  Created by Cameron Carmichael Alonso on 01/06/2014.
//  Copyright (c) 2014 Cameron Carmichael Alonso. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SphericalUtilDelegate <NSObject>
typedef void (^SphericalUtilCompletionBlock)(double returnedLatitude, double returnedLongitude);
-(void)computeOffsetWithLatitude:(double)lat longitude:(double)lng distance:(double)distance heading:(double)heading withCompletion:(SphericalUtilCompletionBlock)callback;

@end

@interface SphericalUtil : NSObject

@property (nonatomic,assign) id<SphericalUtilDelegate> delegate;

-(void)computeOffsetWithLatitude:(double)lat longitude:(double)lng distance:(double)distance heading:(double)heading withCompletion:(SphericalUtilCompletionBlock)callback;

@end
