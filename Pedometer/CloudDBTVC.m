//
//  CloudDBTVC.m
//  Pedometer
//
//  Created by Efdal Ustaoglu on 05.01.18.
//  Copyright © 2018 Ariadne TUM. All rights reserved.
//

#import "CloudDBTVC.h"

@interface CloudDBTVC ()

@end

@implementation CloudDBTVC
@synthesize datas;

- (void)viewDidLoad {
    [super viewDidLoad];
    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc]initWithTitle:@"Generate" style:UIBarButtonItemStyleDone target:self action:@selector(generateValue)];
    self.navigationItem.rightBarButtonItem = rightBtn;
}

- (void)viewDidAppear:(BOOL)animated {
    
    self.ref = [[FIRDatabase database] reference];
    
    FIRDatabaseReference *_refHandle =  [[FIRDatabase database] reference];
    [_refHandle observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        datas = [[NSMutableArray alloc] init];
        NSDictionary *postDict = snapshot.value;
        NSLog(@"Dict:%@",postDict);
        for (NSDictionary *blt in [[postDict valueForKey:@"Users"] allObjects]) {
            NSLog(@"->%@",blt);
            if ([[blt valueForKey:@"timestamp"] doubleValue] < 1515687574) {
                [_refHandle removeValue];
            } else
                [datas addObject:blt];
        }
        [self.tableView reloadData];
    }];
}

- (NSString *)generateRandomValue {
    
    NSString *min = @"-100";
    NSString *max = @"-60";
    int randNum = arc4random() % ([max intValue] - [min intValue]) + [min intValue];
    return [NSString stringWithFormat:@"%d", randNum];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [datas count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Firebase";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    cell.textLabel.text = F(@"X:%@ - Y:%@ - Z:%d", [[datas objectAtIndex:indexPath.row] valueForKey:@"lat"]
                                                 , [[datas objectAtIndex:indexPath.row] valueForKey:@"lng"]
                                                 , [[[datas objectAtIndex:indexPath.row] valueForKey:@"Z-Value"] intValue]);
    cell.detailTextLabel.text = F(@"%@", [[datas objectAtIndex:indexPath.row] valueForKey:@"timestamp"]);
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"" message:@"Are You Sure Want to Logout!" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okButton = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) { [tableView deselectRowAtIndexPath:indexPath animated:YES]; }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}


@end
