//
//  LocationUtils.m
//  Pedometer
//
//  Created by Efdal Ustaoglu on 17.12.17.
//  Copyright © 2018 Ariadne TUM. All rights reserved.
//

#import "LocationUtils.h"

@implementation LocationUtils
@synthesize targetPoints, targetPointsTagged, routablePath;

#pragma mark - Indoor Path Functions

- (GeoJSONDijkstra *)processIndoorPathFeatures:(NSDictionary *)geoJSON :(GeoJSONDijkstra *)geoJSONDijkstra  {
    for ( NSDictionary *feature in [geoJSON valueForKey:@"features"] ) {
        if ([feature valueForKey:@"geometry"] != nil) {
            if ([[feature valueForKey:@"geometry"] valueForKey:@"type"] != nil &&
                [[[feature valueForKey:@"geometry"] valueForKey:@"type"] isEqualToString:@"LineString"]) {
                NSString *elementType = [[feature valueForKey:@"properties"] valueForKey:@"highway"];
                if ([elementType isEqualToString:@"corridor"]) {
                    //add to the routable path
                    //handleLineString(feature, corridors, corridorsInLevels);
                } else if ([elementType isEqualToString:@"steps"]) {
                    //add to the routable path
                    //handleLineString(feature, steps, stepsInLevels);
                }
                //create function that returns an array that contains LatLng objects in the LineString coordinates
                [routablePath addObject:[self lineStringToLatLng:feature]];
            } else if ([[feature valueForKey:@"geometry"] valueForKey:@"type"] != nil &&
                       [[[feature valueForKey:@"geometry"] valueForKey:@"type"] isEqualToString:@"Point"]) {
                [self handleTargetPoint:feature];
            }
        }
    }
    
    geoJSONDijkstra = [[GeoJSONDijkstra alloc] init];
    [geoJSONDijkstra fromGeoJSONToGraph:routablePath];
    return geoJSONDijkstra;
}

- (void)handleTargetPoint:(NSDictionary *)feature {
    
    LatLng *latlng = [[LatLng alloc] init];
    latlng.longitude = [[[feature valueForKey:@"geometry"] valueForKey:@"coordinates"] objectAtIndex:0];
    latlng.latitute  = [[[feature valueForKey:@"geometry"] valueForKey:@"coordinates"] objectAtIndex:1];
    
    NSString *level = @"";
    NSString *identifier = @"";
    NSString *roomName = @"";
    
    level = [[feature valueForKey:@"properties"] valueForKey:@"level"];
    identifier = [[feature valueForKey:@"properties"] valueForKey:@"id"];
    roomName = [[feature valueForKey:@"properties"] valueForKey:@"id"];
    
    if (![level isEqualToString:@""] && identifier != nil) {
        LatLngWithTags *taggedPoint = [[LatLngWithTags alloc] init];
        taggedPoint.level = level;
        taggedPoint.identifier = identifier;
        taggedPoint.latlng = latlng;
        [targetPointsTagged addObject:taggedPoint];
    }
}

- (NSMutableArray *)lineStringToLatLng:(NSDictionary *)feature {
    NSMutableArray *latlngArray = [[NSMutableArray alloc] init];
    
    for (NSDictionary *sub_feature in [[feature valueForKey:@"geometry"] valueForKey:@"coordinates"]) {
        
        LatLngWithTags *latlngObj = [[LatLngWithTags alloc] init];
        latlngObj.latlng = [[LatLng alloc] init];
        latlngObj.level = [[feature valueForKey:@"properties"] valueForKey:@"level"];
        int i = 0;
        for (NSString *location in sub_feature) {
            if      (i == 0) latlngObj.latlng.longitude    = location;
            else if (i == 1) latlngObj.latlng.latitute     = location;
            ++i;
        }
        i = 0;
        latlngObj.identifier = [NSString stringWithFormat:@"%.6f,%.6f", [latlngObj.latlng.longitude doubleValue] , [latlngObj.latlng.latitute doubleValue]];
        [latlngArray addObject:latlngObj];
    }
    return latlngArray;
}

- (LatLngWithTags *)findDestinationFromId:(NSString *)destinationId {
    
    if ([destinationId isEqualToString:@"Main entrance"]) {
        destinationId = @"entrance";
    }
    
    for (LatLngWithTags *candidate in targetPointsTagged) {
        if ([[candidate getIdentifier] isEqualToString:destinationId]) {
            return candidate;
        }
    }
    return nil;
}

- (LatLngWithTags *)setupStartingPoint:(NSString *)lat :(NSString *)lng :(NSString *)level :(NSString *)roomName{
    LatLngWithTags *obj = [[LatLngWithTags alloc] init];
    obj.latlng = [[LatLng alloc] init];
    obj.latlng.latitute = [NSString stringWithFormat:@"%.6f",[lat doubleValue]];
    obj.latlng.longitude = [NSString stringWithFormat:@"%.6f",[lng doubleValue]];
    obj.level = [NSString stringWithFormat:@"%@",level];
    obj.identifier = [NSString stringWithFormat:@"%@",roomName];
    return obj;
}

- (MKMapView *)handleRouteRequest :(MKMapView *)mapView : (int)drawStyle :(GeoJSONDijkstra *)geoJSONDijkstra :(LatLngWithTags *) startingPoint :(NSString *)roomId {
    
    NSString *destinationId = @"00.09.037";
    if ([roomId length] > 0) {
        destinationId = roomId;
    }
    LatLngWithTags *destinationLatLng = [self findDestinationFromId:destinationId];
    if (destinationLatLng != nil){
        PESGraphRoute *route = [geoJSONDijkstra startDijkstra:startingPoint :destinationLatLng];
        MKPolyline *routeLine = [self createPolylineFromRoute:route];
        drawStyle = DrawRoute;
        [mapView addOverlay:routeLine];
    }
    return mapView;
}

- (GMSMapView *)handleRouteRequest2 :(GMSMapView *)mapView : (int)drawStyle :(GeoJSONDijkstra *)geoJSONDijkstra :(LatLngWithTags *) startingPoint :(NSString *)roomId {
    NSString *destinationId = @"entrance";

    if ([roomId length] > 0) {
        destinationId = roomId;
    }
    
    if ([roomId isEqualToString:@"Main entrance"]) {
        roomId = @"entrance";
    }
    
    LatLngWithTags *destinationLatLng = [self findDestinationFromId:destinationId];
    if (destinationLatLng != nil){
        NSLog(@"not empty");
        PESGraphRoute *route = [geoJSONDijkstra startDijkstra:startingPoint :destinationLatLng];
        NSMutableDictionary *routesInLevels = [[NSMutableDictionary alloc] init];
        
        for (PESGraphRouteStep * pgrs in route.steps){
            if (pgrs.node.title != nil) {
                if (![pgrs.node.title containsString:@";"]) {
                    if([routesInLevels valueForKey:pgrs.node.title] != nil) {
                        //key exists
                        NSLog(@"------>%@",pgrs.node.title);
                        [routesInLevels[pgrs.node.title] addObject:pgrs];
                    } else {
                        NSMutableArray *levelArray = [[NSMutableArray alloc] init];
                        [routesInLevels setObject:levelArray forKey:pgrs.node.title];
                        [routesInLevels[pgrs.node.title] addObject:pgrs];
                    }
                }
                else {// add the stairs to the lower floor , first split with ";" , then get the lower floor id and add it
                    //NSLog(@"TITLE stairs:%@",pgrs.node.title);
                    NSArray *splittedArr = [pgrs.node.title componentsSeparatedByString:@";"];
                    
                    if([routesInLevels valueForKey:splittedArr[0]] != nil) {
                        //key exists
                        [routesInLevels[splittedArr[0]] addObject:pgrs];
                    } else {
                        NSMutableArray *levelArray = [[NSMutableArray alloc] init];
                        [routesInLevels setObject:levelArray forKey:splittedArr[0]];
                        [routesInLevels[splittedArr[0]] addObject:pgrs];
                    }
                    //[routesInLevels[splittedArr[0]] addObject:pgrs];
                }
            }
        }
    
        GMSPolyline *routeLine = [self createPolylineFromRoute:route];
        NSMutableArray *routeArray = [self createPolylineInLevelsFromRoute:routesInLevels];
        drawStyle = DrawRoute;
        
        for(GMSPolyline *line in routeArray)
            line.map = mapView;
        //routeLine.map = mapView;
        NSLog(@"-->%@",routeLine);
        
        //[[NSNotificationCenter defaultCenter] postNotificationName:@"GMSMapObject" object:routeLine userInfo:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GMSMapArray" object:routeArray userInfo:nil];

    } else
        NSLog(@"empty");
    
    return mapView;
}

- (GMSPolyline *)createPolylineFromRoute: (PESGraphRoute *)route {
    
    CLLocationCoordinate2D coordinateArray[route.steps.count];
    GMSMutablePath *path = [GMSMutablePath path];
    for (int i = 0; i < route.steps.count; i++) {
        
        NSArray *latLngInPath = [[[route.steps[i] node] identifier] componentsSeparatedByString:@","];
        CLLocationCoordinate2D coord = CLLocationCoordinate2DMake([latLngInPath[1] doubleValue], [latLngInPath[0] doubleValue]);
        coordinateArray[i] = coord;
        [path addCoordinate:coord];
    }
    GMSPolyline *routeLine = [[GMSPolyline alloc] init];
    routeLine.strokeWidth = 2;
    routeLine.strokeColor = [UIColor redColor];
    routeLine.path = path;
    return routeLine;
}

- (NSMutableArray *)createPolylineInLevelsFromRoute: (NSMutableDictionary *)routesInLevels {
    
    //sort routes in level, otherwise level 0 & level 3 are mixed
    NSArray *keys = [routesInLevels allKeys];
    NSArray *sortedKeys = [keys sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES]]];
    NSMutableArray *routeArray = [[NSMutableArray alloc] init];
    NSMutableArray *stairsArray = [[NSMutableArray alloc] init];
    
    for ( int i=0 ; i<sortedKeys.count ; i++){
        NSMutableArray *graphRoute = [routesInLevels valueForKey:sortedKeys[i]];
        GMSMutablePath *path = [GMSMutablePath path];
        for (int j = 0; j < [graphRoute count]; j++) {
            NSArray *latLngInPath = [[[[graphRoute objectAtIndex:j] node] identifier] componentsSeparatedByString:@","];
            CLLocationCoordinate2D coord = CLLocationCoordinate2DMake([latLngInPath[1] doubleValue], [latLngInPath[0] doubleValue]);
            [path addCoordinate:coord];
            
            if ([[[[graphRoute objectAtIndex:j] node] title] containsString:@";"]) {//it's a stair
                [stairsArray addObject:[graphRoute objectAtIndex:j]];
            }
        }
        GMSPolyline *routeLine = [[GMSPolyline alloc] init];
        routeLine.strokeWidth = 2;
        routeLine.strokeColor = [UIColor grayColor];
        routeLine.path = path;
        [routeArray addObject:routeLine];
    }
    //with nsnotification send the stairsArray
    
    return routeArray;
}




/*
- (MKPolyline *)createPolylineFromRoute: (PESGraphRoute *)route {
    
    CLLocationCoordinate2D coordinateArray[route.steps.count];
    for (int i = 0; i < route.steps.count; i++) {
        
        NSArray *latLngInPath = [[[route.steps[i] node] identifier] componentsSeparatedByString:@","];
        CLLocationCoordinate2D coord = CLLocationCoordinate2DMake([latLngInPath[1] doubleValue], [latLngInPath[0] doubleValue]);
        coordinateArray[i] = coord;
    }
    MKPolyline *routeLine = [MKPolyline polylineWithCoordinates:coordinateArray count:route.steps.count];
    return routeLine;
}
*/

@end
