//
//  LatLng.h
//  Pedometer
//
//  Created by Efdal Ustaoglu on 12.12.17.
//  Copyright © 2018 Ariadne TUM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LatLng : NSObject

@property (nonatomic, assign) NSString *latitute;
@property (nonatomic, assign) NSString *longitude;

@end
