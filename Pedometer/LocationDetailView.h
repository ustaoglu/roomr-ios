//
//  LocationDetailView.h
//  Pedometer
//
//  Created by Efdal Ustaoglu on 09.04.18.
//  Copyright © 2018 Navindo GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationDetailView : UIView

@property (strong, nonatomic) IBOutlet UILabel *destinationNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *destinationAddrLabel;
@property (strong, nonatomic) IBOutlet UIView *bottomSquareView;
@property (strong, nonatomic) IBOutlet UIButton *bottomSquareButton;

- (void)initWithNameAndAddr:(NSString *)nameString andAddr:(NSString *)addrString;
@end
