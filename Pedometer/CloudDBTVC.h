//
//  CloudDBTVC.h
//  Pedometer
//
//  Created by Efdal Ustaoglu on 05.01.18.
//  Copyright © 2018 Ariadne TUM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Firebase/Firebase.h>
#import "Macros.h"

@interface CloudDBTVC : UITableViewController

@property (nonatomic, strong) NSMutableArray *datas;

@property (strong, nonatomic) FIRDatabaseReference *ref;

@end
