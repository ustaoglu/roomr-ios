//
//  ViewController.h
//  Pedometer
//
//  Created by Jay Versluis on 29/10/2015.
//  Copyright © 2018 Ariadne TUM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>
#import "Imports.h"
#import "MapOverlayView.h"
//@import Mapbox;
#import <math.h>
#import <Firebase/Firebase.h>
#import "Macros.h"
#import <UIDevice+FCUUID.h>
#import "PositionEstimateService.h"
#import <AFNetworking/AFNetworking.h>

float stepsTillNow = 0;
double walkingAngle = 99.0;
double STEP_LENGTH_CORRIDOR = 0.67;
double STEP_LENGTH_ESCALATOR = 0.40;
double stepSize = 0.67;

double currentAccelX;
double currentAccelY;
double currentAccelZ;
double currentRotX;
double currentRotY;
double currentRotZ;

@interface ViewController : UIViewController<CLLocationManagerDelegate/*,MGLMapViewDelegate*/, MKMapViewDelegate, UITextFieldDelegate, PositionEstimateServiceProtocol> {
    CLLocationManager *locationManager;
    LocationUtils *locUtil;
}

@property (readwrite, nonatomic, strong) MKMapView *mapView2;
@property (nonatomic, strong) GeoJSONDijkstra *geoJSONDijkstra;

@property (strong, nonatomic) IBOutlet UIView *roundedView;
@property (strong, nonatomic) IBOutlet MKMapView *myMapView;
@property (strong, nonatomic) IBOutlet UITextField *myTextField;

@property (strong, nonatomic) IBOutlet UILabel *compassLabel;
@property (strong, nonatomic) CMMotionManager *motionManager;

@property (strong, nonatomic) FIRDatabaseReference *ref;

- (IBAction)increaseFirebaseParentAction:(id)sender;

- (IBAction)sendPositionAction:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *estimatedPositionLabel;

@end

@interface NSDictionary (BVJSONString)
-(NSString*) bv_jsonStringWithPrettyPrint:(BOOL) prettyPrint;
@end

@implementation NSDictionary (BVJSONString)

-(NSString*) bv_jsonStringWithPrettyPrint:(BOOL) prettyPrint {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self
                                                       options:(NSJSONWritingOptions)    (prettyPrint ? NSJSONWritingPrettyPrinted : 0)
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"bv_jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
        return @"{}";
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}
@end
