//
//  LocationDetailView.m
//  Pedometer
//
//  Created by Efdal Ustaoglu on 09.04.18.
//  Copyright © 2018 Navindo GmbH. All rights reserved.
//

#import "LocationDetailView.h"

@implementation LocationDetailView

- (void)initWithNameAndAddr:(NSString *)nameString andAddr:(NSString *)addrString {
    self.destinationNameLabel.text = nameString;
    self.destinationAddrLabel.text = addrString;
    self.bottomSquareView.layer.cornerRadius = self.bottomSquareView.frame.size.height / 2;
}

@end
