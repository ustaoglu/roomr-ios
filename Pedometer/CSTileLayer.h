//
//  CSTileLayer.h
//  Pedometer
//
//  Created by HiWi on 05.06.18.
//  Copyright © 2018 Navindo GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GoogleMaps/GoogleMaps.h>
#import <UIKit/UIKit.h>
#import "Macros.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SSZipArchive.h"
@protocol CSTileLayerDelegate <NSObject>
- (void)didFinishLoadingTile:(UIImage *)image;
@end

@interface CSTileLayer : GMSSyncTileLayer

+ (instancetype)sharedInstance;
@property (nonatomic, retain) id <CSTileLayerDelegate> delegate;
@property (nonatomic, strong) NSString *tileLevel;

@end



