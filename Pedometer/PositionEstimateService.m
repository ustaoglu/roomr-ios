//
//  PositionEstimateService.m
//  Pedometer
//
//  Created by Efdal Ustaoglu on 15.02.18.
//  Copyright © 2018 Navindo GmbH. All rights reserved.
//

#import "PositionEstimateService.h"

@implementation PositionEstimateService{
    NSMutableData *_downloadedData;
}

- (void)estimatePosition:(NSMutableDictionary *)positionDict{
    
    NSString *url = [NSString stringWithFormat:@"http://141.40.254.103:8888"];
    NSURL *jsonFileUrl = [NSURL URLWithString:url];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:jsonFileUrl cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];
    [urlRequest setHTTPMethod:@"POST"];

    NSString *postString = F(@"%@", positionDict);
    [urlRequest setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
//    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];

    NSLog(@"postString = %@", postString);
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // Initialize the data object
    _downloadedData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the newly downloaded data
    [_downloadedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSString *myString = [[NSString alloc] initWithData:_downloadedData encoding:NSUTF8StringEncoding];
    NSLog(@"response->%@",myString);
    
    // Parse the JSON that came in
    NSError *error;
    NSMutableArray *topLevelArray = [NSJSONSerialization JSONObjectWithData:_downloadedData options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&error];
    NSLog(@"%@", topLevelArray);
    [self.delegate outputReceived:topLevelArray];
}

@end
