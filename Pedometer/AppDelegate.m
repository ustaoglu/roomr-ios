//
//  AppDelegate.m
//  Pedometer
//
//  Created by Jay Versluis on 29/10/2015.
//  Copyright © 2018 Ariadne TUM. All rights reserved.
//

#import "AppDelegate.h"
@import GoogleMaps;

@interface AppDelegate ()

@end

@implementation AppDelegate

-(void) copytoDocument {
    NSString *openFile ;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *pgnPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"Tiles"]];
    
    if ([fileManager fileExistsAtPath:pgnPath] == NO) {
        NSString *resourcePath = [[NSBundle mainBundle] pathForResource:@"Tiles" ofType:@""];
        [fileManager copyItemAtPath:resourcePath toPath:pgnPath error:&error];
        if (error) {
            NSLog(@"Error on copying file: %@\nfrom path: %@\ntoPath: %@", error, resourcePath, pgnPath);
        }
    }
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.

//    [AWSDDLog addLogger:[AWSDDTTYLogger sharedInstance]];
//    [[AWSDDLog sharedInstance] setLogLevel:AWSDDLogLevelInfo];
    [FIRApp configure];
    [self copytoDocument];
    [GMSServices provideAPIKey:@"AIzaSyBLRszF26pu5zFmDI5yCFfrl0q7OmNk2Nk"];
    //[GMSPlacesClient provideAPIKey:@"AIzaSyBLRszF26pu5zFmDI5yCFfrl0q7OmNk2Nk"];
    
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    if ([pref valueForKey:@"fireCount"] == nil) {
        [pref setValue:@"1" forKey:@"fireCount"];
        [pref synchronize];
    }
    
//    return [[AWSMobileClient sharedInstance] interceptApplication:application didFinishLaunchingWithOptions:launchOptions];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
