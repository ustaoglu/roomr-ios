//
//  GeoJSONDijkstra.m
//  Pedometer
//
//  Created by Efdal Ustaoglu on 12.12.17.
//  Copyright © 2018 Ariadne TUM. All rights reserved.
//

#import "GeoJSONDijkstra.h"

@implementation GeoJSONDijkstra

- (void)newVertexFromPoint: (LatLngWithTags *)sendPoint{
    
//    NSString *level = [sendPoint level];
//    NSString *identifier = [sendPoint identifier];
    //return Vertex obj with (id, id, level);
}


- (void)addNewVertex :(NSMutableArray *)vertices :(NSMutableSet *)nameSet :(PESGraphNode *)vertex {
    
    if (![nameSet containsObject:vertex.identifier]) {
        [nameSet addObject:vertex.identifier];
        [vertices addObject:vertex];
    }
}

- (void)fromGeoJSONToGraph: (NSMutableArray *)routablePaths {
    
    vertices = [[NSMutableArray alloc] init];
    edges = [[NSMutableArray alloc] init];
    graph = [[PESGraph alloc] init];
    NSMutableSet *s = [[NSMutableSet alloc ]init];

    for (NSMutableArray *latlngArray in routablePaths) {
        for (int i = 0; i < latlngArray.count-1; i++) {
            
            PESGraphNode *source = [[PESGraphNode alloc] init];
            source.identifier = [latlngArray[i] identifier];
            source.title = [latlngArray[i] getLevel];
            
            PESGraphNode *destination = [[PESGraphNode alloc] init];
            destination.identifier = [latlngArray[i+1] identifier];
            destination.title = [latlngArray[i+1] getLevel];

            CLLocation *locA = [[CLLocation alloc] initWithLatitude:[[[latlngArray[i] latlng] latitute] doubleValue] longitude:[[[latlngArray[i] latlng] longitude] doubleValue]];
            
            CLLocation *locB = [[CLLocation alloc] initWithLatitude:[[[latlngArray[i+1] latlng] latitute] doubleValue] longitude:[[[latlngArray[i+1] latlng] longitude] doubleValue]];
            
            CLLocationDistance distance = [locA distanceFromLocation:locB];
            
            [self addNewVertex:vertices :s :source];
            [graph addBiDirectionalEdge:[PESGraphEdge edgeWithName:[NSString stringWithFormat:@"%@-%@",source.identifier,destination.identifier]
                                                         andWeight:[NSNumber numberWithDouble:distance]] fromNode:source toNode:destination];
            if (i == latlngArray.count-2) {
                [self addNewVertex:vertices :s :destination];
            }
        }
    }
}

- (PESGraphRoute *)startDijkstra :(LatLngWithTags *)source :(LatLngWithTags *)destination {
    
    int index_s = [self findVertexIndex:[NSString stringWithFormat:@"%.6f,%.6f", [source.latlng.longitude doubleValue], [source.latlng.latitute doubleValue]]];//[source identifier]];
    int index_d = [self findVertexIndex:[NSString stringWithFormat:@"%.6f,%.6f", [destination.latlng.longitude doubleValue] , [destination.latlng.latitute doubleValue]]];
    if (index_s > 0 && index_d > 0) {
        
        PESGraphRoute *route = [graph shortestRouteFromNode:vertices[index_s] toNode:vertices[index_d]];
        return route;
        NSLog(@"Route count:%lu",[route count]);
    } else {
        NSLog(@"Dijkstra starting point not found!");
        return nil;
    }
}

/*
public void startDijkstra(LatLngWithTags source) {
    int index = findVertexIndex(source);
    if (index >= 0) {
        dijkstraAlgorithm.execute(vertices.get(index));
        ArrayList<Vertex> nodesWithoutpredecessors = dijkstraAlgorithm.getNodesWithoutPredecessors();
        //            System.out.println("Start dijkstra from: " + vertices.get(index).getId());
    } else {
        System.out.println("Dijkstra starting point not found!!!!!!");
    }
}
*/
 
- (int)findVertexIndex :(NSString *)source {
    int index = -1;
    for (int i = 0; i < vertices.count -1; i++) {
        if ([[vertices[i] identifier] isEqualToString:source]) {
            level = [vertices[i] title];
            index = i;
        }
    }
    return index;
}


@end
