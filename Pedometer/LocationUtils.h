//
//  LocationUtils.h
//  Pedometer
//
//  Created by Efdal Ustaoglu on 17.12.17.
//  Copyright © 2018 Ariadne TUM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Imports.h"
#import <GoogleMaps/GoogleMaps.h>

@interface LocationUtils : NSObject

@property (nonatomic, strong) NSMutableArray * routablePath;
@property (nonatomic, strong) NSMutableArray * targetPoints;
@property (nonatomic, strong) NSMutableArray * targetPointsTagged;

- (GeoJSONDijkstra *)processIndoorPathFeatures:(NSDictionary *)geoJSON :(GeoJSONDijkstra *)geoJSONDijkstra;
- (void)handleTargetPoint:(NSDictionary *)feature;
- (NSMutableArray *)lineStringToLatLng:(NSDictionary *)feature;
- (LatLngWithTags *)findDestinationFromId:(NSString *)destinationId;
- (LatLngWithTags *)setupStartingPoint:(NSString *)lat :(NSString *)lng :(NSString *)level :(NSString *)roomName;
- (MKMapView *)handleRouteRequest :(MKMapView *)mapView : (int)drawStyle :(GeoJSONDijkstra *)geoJSONDijkstra :(LatLngWithTags *) startingPoint :(NSString *)roomId;
- (GMSMapView *)handleRouteRequest2 :(GMSMapView *)mapView : (int)drawStyle :(GeoJSONDijkstra *)geoJSONDijkstra :(LatLngWithTags *) startingPoint :(NSString *)roomId;
//- (MKPolyline *)createPolylineFromRoute: (PESGraphRoute *)route;
- (GMSPolyline *)createPolylineFromRoute: (PESGraphRoute *)route;

@end
