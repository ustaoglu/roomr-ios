//
//  MapVC.h
//  Pedometer
//
//  Created by Efdal Ustaoglu on 29.03.18.
//  Copyright © 2018 Navindo GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Imports.h"
#import <GoogleMaps/GoogleMaps.h>
#import "GMUGeoJSONParser.h"
#import "GMUGeometryRenderer.h"
#import "Macros.h"
#import "LocationDetailView.h"
#import "MapRouteVC.h"
#import "MapSearchVC.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "WMSTileLayer.h"
#import "CSTileLayer.h"
#import "BuildingBox.h"

@interface MapVC : UIViewController<GMSMapViewDelegate, UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIView *myMapView;

@property (strong, nonatomic) IBOutlet UIView *bottomLeftView;
@property (strong, nonatomic) IBOutlet UIImageView *bottomLeftImageView;
@property (strong, nonatomic) IBOutlet UIButton *bottomLeftButton;

@property (nonatomic, strong) GeoJSONDijkstra *geoJSONDijkstra;

@property (nonatomic, strong) GMSMapView *mapView;
@property (nonatomic, strong) WMSTileLayer *wmsTileLayer;

@end
