//
//  GeoJSONDijkstra.h
//  Pedometer
//
//  Created by Efdal Ustaoglu on 12.12.17.
//  Copyright © 2018 Ariadne TUM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LatLngWithTags.h"

//Dijkstra Algo Functions
#import "PESGraph.h"
#import "PESGraphNode.h"
#import "PESGraphEdge.h"
#import "PESGraphRoute.h"
#import "PESGraphRouteStep.h"

#import <CoreLocation/CoreLocation.h>
#import "LatLngWithTags.h"

@interface GeoJSONDijkstra : NSObject{
    
    NSMutableArray *vertices;
    NSMutableArray *edges;
    NSString *level;
    PESGraph *graph;
}

- (void)fromGeoJSONToGraph: (NSMutableArray *)routablePaths;
- (PESGraphRoute *)startDijkstra :(LatLngWithTags *)source :(LatLngWithTags *)destination;

@end
