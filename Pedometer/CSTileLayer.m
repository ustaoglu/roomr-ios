//
//  CSTileLayer.m
//  Pedometer
//
//  Created by HiWi on 05.06.18.
//  Copyright © 2018 Navindo GmbH. All rights reserved.
//

#import "CSTileLayer.h"

@implementation CSTileLayer
@synthesize tileLevel;

+ (instancetype)sharedInstance
{
    static CSTileLayer *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[CSTileLayer alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedInstance;
}

+ (NSString *)applicationDocumentDirectory {
    
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

+ (NSString *)applicationDocumentPrivateDocumentsDirectory {
    
    NSError *error;
    NSString *PrivateDocumentsDirectory = [[self applicationDocumentDirectory] stringByAppendingPathComponent:@"Tiles"];
//    BOOL isDir;
//    if (! [[NSFileManager defaultManager] fileExistsAtPath:PrivateDocumentsDirectory isDirectory:&isDir]) {
//
//        if (![[NSFileManager defaultManager] createDirectoryAtPath:PrivateDocumentsDirectory
//                                       withIntermediateDirectories:NO
//                                                        attributes:nil
//                                                             error:&error]) {
//            NSLog(@"Create directory error: %@", error);
//        }
//    }
    
    return PrivateDocumentsDirectory;
}

- (UIImage *)tileForX:(NSUInteger)x y:(NSUInteger)y zoom:(NSUInteger)zoom {

//    NSFileManager *fm = [[NSFileManager alloc] init];
//    NSString *zipPath = [[NSBundle mainBundle] pathForResource:@"tiles" ofType:@"zip"];
    
    return [self loadImage:x :y :zoom];
    
    
//    NSString *destinationPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
//
//    NSLog(@"----->%@",[CSTileLayer applicationDocumentDirectory]);
//    NSLog(@"----->%@",[CSTileLayer applicationDocumentPrivateDocumentsDirectory]);
//
//    BOOL isDir;
//    BOOL exists = [fm fileExistsAtPath:[NSString stringWithFormat:@"%@/Tiles",destinationPath] isDirectory:&isDir];
//    if (exists) {
//        /* file exists */
//        if (isDir) {
//            /* file is a directory */
//            NSLog(@"directory exists");
//            //give path and return image
//            return [self loadImage:x :y :zoom];
//        } else {
//            [SSZipArchive unzipFileAtPath:zipPath toDestination:destinationPath];
//            NSLog(@"%@",destinationPath);
//        }
//        return kGMSTileLayerNoTile;
//    }
    
    
//    // On every odd tile, render an image.
//    if (x % 2) {
//        return [UIImage imageNamed:@"germany"];
//    } else {
//        return kGMSTileLayerNoTile;
//    }

//    UIImage *tileImage = [self loadImage:x :y :zoom];
//
//    CGImageRef cgref = [tileImage CGImage];
//    CIImage *cim = [tileImage CIImage];
//
//    if (cim != nil && cgref != NULL) {
//        NSLog(@"yes image");
//        return tileImage;
//    }
    
    //check db if not exist
    //return kGMSTileLayerNoTile
    
    //else return image
    
//    NSString *url = [NSString stringWithFormat:@"%@",BASE_TILE_URL];
//    url = [url stringByAppendingString:[NSString stringWithFormat:@"/hot%@/%tu/%tu/%tu.png", tileLevel, zoom, x, y]];
//    [self downloadImage:url];
    
    
    
//    url = [url stringByAppendingString:[NSString stringWithFormat:@"/hot1/%tu/%tu/%tu.png", zoom, x, y]];
//
////    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"germany.png"]];
////    [imageView sd_setImageWithURL:[NSURL URLWithString:url]
////                 placeholderImage:[UIImage imageNamed:@"germany.png"]];
//
//    // On every odd tile, render an image.
//    if (imageView.image != nil) {
//        return imageView.image;
//    } else {
        return kGMSTileLayerNoTile;
//    }
}





- (void)downloadImage: (NSString *)url {
    
    NSString *imageWithoutBaseUrl = [NSString stringWithFormat:url];
    imageWithoutBaseUrl = [imageWithoutBaseUrl stringByReplacingOccurrencesOfString:BASE_TILE_URL withString:@""];
    imageWithoutBaseUrl = [imageWithoutBaseUrl stringByReplacingOccurrencesOfString:@"hot" withString:@""];
    NSLog(@"->%@",imageWithoutBaseUrl);
    NSArray *imageParts = [imageWithoutBaseUrl componentsSeparatedByString:@"/"];
    NSLog(@"array:%@",imageParts);
    //zoom, x, y
    
    //download the image and call saveImage
    NSURLSessionDownloadTask *downloadPhotoTask = [[NSURLSession sharedSession]
                                                   downloadTaskWithURL:[NSURL URLWithString:url] completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
                                                       UIImage *downloadedImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:location]];
                                                       [self saveImage:downloadedImage
                                                                      :[imageParts[3] intValue]
                                                                      :[imageParts[4] intValue]
                                                                      :[imageParts[2] intValue]];
                                                   }];
    [downloadPhotoTask resume];
    
}

- (void)saveImage: (UIImage*)image :(int)x :(int)y :(int)zoom {

    if (image != nil) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *path;
//        if ([tileLevel intValue] != 0) {
            path = [documentsDirectory stringByAppendingPathComponent:F(@"TILES/%@/%d/%d/%d.png", tileLevel, zoom, x, y)];
//        }
//        else {
//            path = [documentsDirectory stringByAppendingPathComponent:F(@"TILES/0/%tu/%tu/%tu.png", zoom, x, y)];
//        }
        NSLog(@"PATH1:%@",path);
        NSData *data = UIImagePNGRepresentation(image);
        [data writeToFile:path atomically:YES];
        [self.delegate didFinishLoadingTile:image];
    }
}

- (UIImage*)loadImage :(NSUInteger *)x :(NSUInteger *)y :(NSUInteger *)zoom {

    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
//    if ([tileLevel intValue] != 0) {
    int myZoom = (int)zoom;
    NSLog(@"myzoom:%d    -    incoming zoom:%tu",myZoom, zoom);
        path = [path stringByAppendingPathComponent:F(@"Tiles/%d/%@/%tu/%tu.png", myZoom, tileLevel, x, y)];
    NSLog(@"PATH2:%@",path);
//    }
//    else {
//        path = [documentsDirectory stringByAppendingPathComponent:F(@"TILES/0/%tu/%tu/%tu.png", zoom, x, y)];
//    }
    return [UIImage imageWithContentsOfFile:path];;
}



@end
