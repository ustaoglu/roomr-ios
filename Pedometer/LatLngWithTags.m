//
//  LatLngWithTags.m
//  Pedometer
//
//  Created by Efdal Ustaoglu on 10.12.17.
//  Copyright © 2018 Ariadne TUM. All rights reserved.
//

#import "LatLngWithTags.h"

@implementation LatLngWithTags

- (NSString *)toString {
    return [NSString stringWithFormat:@"id : %@, level: %@, location: %@",self.identifier, self.level, self.latlng];
}

- (NSString *)getLevel{
    return self.level;
}

- (NSString *)getIdentifier{
    return self.identifier;
}

@end
