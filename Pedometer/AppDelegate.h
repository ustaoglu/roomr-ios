//
//  AppDelegate.h
//  Pedometer
//
//  Created by Jay Versluis on 29/10/2015.
//  Copyright © 2018 Ariadne TUM. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <AWSMobileClient/AWSMobileClient.h>
//#import <AWSCore/AWSCore.h>

@import Firebase;

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;

@end

