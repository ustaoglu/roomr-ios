//
//  Constants.h
//
//  Created by Efdal Ustaoglu on 29/06/18.
//  Copyright © 2018 Navindo. All rights reserved.
//

///////////////////////////////////////////
// App
///////////////////////////////////////////

#define APP_VERSION [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"]
#define APP_BUILD_VERSION [[NSBundle mainBundle] infoDictionary][@"CFBundleVersion"]


///////////////////////////////////////////
// Views
///////////////////////////////////////////
#define WIDTH(view) view.frame.size.width
#define HEIGHT(view) view.frame.size.height
#define X(view) view.frame.origin.x
#define Y(view) view.frame.origin.y
#define LEFT(view) view.frame.origin.x
#define TOP(view) view.frame.origin.y
#define BOTTOM(view) (view.frame.origin.y + view.frame.size.height)
#define RIGHT(view) (view.frame.origin.x + view.frame.size.width)

///////////////////////////////////////////
// Device & OS
///////////////////////////////////////////

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

#define Is5_5Inches() ([[UIScreen mainScreen] bounds].size.height == 736)
#define Is4_7Inches() ([[UIScreen mainScreen] bounds].size.height == 667)
#define Is4Inches() ([[UIScreen mainScreen] bounds].size.height == 568)
#define Is3_5Inches() ([[UIScreen mainScreen] bounds].size.height == 480.0f)
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

///////////////////////////////////////////
// Networking
///////////////////////////////////////////
#define IsConnected() !([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable)
#define ShowNetworkActivityIndicator() [UIApplication sharedApplication].networkActivityIndicatorVisible = YES
#define HideNetworkActivityIndicator() [UIApplication sharedApplication].networkActivityIndicatorVisible = NO

///////////////////////////////////////////
// Misc
///////////////////////////////////////////
#define URLIFY(urlString) [NSURL URLWithString:urlString]
#define F(string, args...) [NSString stringWithFormat:string, args]
#define ALERT(title, msg) [[[UIAlertView alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show]
#define BIBDarkredcolor [UIColor colorWithRed:0.39 green:0.07 blue:0.07 alpha:1.0]//new one
#define BIBredcolor [UIColor colorWithRed:110/255.0 green:19/255.0 blue:22/255.0 alpha:1]//old one
#define kOFFSET_FOR_KEYBOARD_BETSLIP 350
#define IOS_DEFINED_APIKEY @"a@AGa23$!abc9"
#define BIB_ROUNDING_BEHAVIOUR [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundPlain scale:2 raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:YES]

#define logIntVariable(x) NSLog( @"Value of %s = %d",#x, x)
#define logStringVariable(x) NSLog( @"Value of %s = %s",#x, x)
#define logFloatVariable(x) NSLog( @"Value of %s = %f",#x, x)

#define mTimeoutInSeconds                           900.0f

///////////////////////////////////////////
// Defaults
///////////////////////////////////////////

#define DefaultPhoneLang ([[[NSLocale componentsFromLocaleIdentifier:[[NSLocale preferredLanguages] objectAtIndex:0]] objectForKey:@"kCFLocaleLanguageCodeKey"] isEqualToString:@"de"]) ? @"de" : @"en"

#define IDIOM    UI_USER_INTERFACE_IDIOM()
#define IPAD     UIUserInterfaceIdiomPad

#define ONESIGNAL_APP_ID                        @"70dde101-46ee-413f-b8d7-5301e75f81b9"

#define GOOGLE_ANALYTICS_ID                     @"UA-92528351-2"

#define DrawRoute 11
#define DrawMap   10

#define DEGREES_TO_RADIANS(degrees)((M_PI * degrees)/180)
#define RADIANS_TO_DEGREES(radians) ((radians) * (180.0 / M_PI))

#define TimeStamp [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]]

#define BASE_TILE_URL                           @"http://ec2-18-188-248-182.us-east-2.compute.amazonaws.com"



