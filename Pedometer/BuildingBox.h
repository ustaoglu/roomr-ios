//
//  BuildingBox.h
//  Pedometer
//
//  Created by HiWi on 11.06.18.
//  Copyright © 2018 Pinkstone Pictures LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BuildingBox : NSObject

@property double min_lat;
@property double min_lng;
@property double max_lat;
@property double max_lng;

@end
