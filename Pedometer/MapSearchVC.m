//
//  MapSearchVC.m
//  Pedometer
//
//  Created by Efdal Ustaoglu on 12.04.18.
//  Copyright © 2018 Navindo GmbH. All rights reserved.
//

#import "MapSearchVC.h"

@interface MapSearchVC () {
    NSMutableArray *roomsArray;
    NSMutableArray *buildingArray;
    NSMutableArray *searchArray;
    NSDictionary *geoJSON;
    BOOL searchActivated;
}

@end

@implementation MapSearchVC
@synthesize myTableView, myTextField;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    searchActivated = FALSE;
    roomsArray = [[NSMutableArray alloc] init];
    buildingArray = [[NSMutableArray alloc] init];
    searchArray = [[NSMutableArray alloc] init];
    myTextField.delegate = self;
    myTextField.keyboardType = UIKeyboardTypeDecimalPad;
    
    //add bar button item over the keyboard
    UIToolbar* keyboardToolbar = [[UIToolbar alloc] init];
    [keyboardToolbar sizeToFit];
    UIBarButtonItem *flexBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                      target:nil action:nil];
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                      target:self action:@selector(yourTextFieldDoneButtonPressed)];
    keyboardToolbar.items = @[flexBarButton, doneBarButton];
    myTextField.inputAccessoryView = keyboardToolbar;

//    NSURL *URL = [[NSBundle mainBundle] URLForResource:@"indoor_path_ga_mi" withExtension:@"geojson"];
//    NSData *data = [NSData dataWithContentsOfURL:URL];
//    NSDictionary *dict1 = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//    geoJSON = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

    NSURL *URL = [[NSBundle mainBundle] URLForResource:@"mi" withExtension:@"geojson"];
    NSData *data = [NSData dataWithContentsOfURL:URL];
    NSDictionary *dict1 = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

    NSURL *URL2 = [[NSBundle mainBundle] URLForResource:@"mw" withExtension:@"geojson"];
    NSData *data2 = [NSData dataWithContentsOfURL:URL2];
    NSDictionary *dict2 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];

    NSURL *URL3 = [[NSBundle mainBundle] URLForResource:@"mc" withExtension:@"geojson"];
    NSData *data3 = [NSData dataWithContentsOfURL:URL3];
    NSDictionary *dict3 = [NSJSONSerialization JSONObjectWithData:data3 options:0 error:nil];
    
    NSMutableArray *dictArray = [[NSMutableArray alloc] init];
    [dictArray addObject:dict1];
    [dictArray addObject:dict2];
    [dictArray addObject:dict3];
    
    for (NSDictionary *dictionary in dictArray)
        for (NSDictionary *dict in [dictionary valueForKey:@"features"]){
            if ([[dict valueForKey:@"properties"] valueForKey:@"id"] /*&& ![[[dict valueForKey:@"properties"] valueForKey:@"id"] isEqualToString:@"entrance"]*/){
                Room *tmp = [[Room alloc] init];
                tmp.room_id = [[dict valueForKey:@"properties"] valueForKey:@"id"];
                tmp.building_id = [[dict valueForKey:@"properties"] valueForKey:@"building_id"];
                tmp.level = [[dict valueForKey:@"properties"] valueForKey:@"level"];
                [roomsArray addObject:tmp];
            }
        }
    
    NSArray *sortedArray;
    sortedArray = [roomsArray sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        NSString *first = [(Room*)a room_id];
        NSString *second = [(Room*)b room_id];
        return [first compare:second];
    }];
    
    roomsArray = [NSMutableArray arrayWithArray:sortedArray];
    
//    if (self.comesFromRouteSelection){//check this part again, might crash;
//        for (NSDictionary *dict in [geoJSON valueForKey:@"features"]){
//            if ([[dict valueForKey:@"properties"] valueForKey:@"id"] && [[[dict valueForKey:@"properties"] valueForKey:@"id"] isEqualToString:@"entrance"]){
//                Room *tmp = [[Room alloc] init];
//                tmp.room_id = [[dict valueForKey:@"properties"] valueForKey:@"id"];
//                tmp.building_id = [[dict valueForKey:@"properties"] valueForKey:@"building_id"];
//                tmp.level = [[dict valueForKey:@"properties"] valueForKey:@"level"];
//                [roomsArray insertObject:tmp atIndex:0];
//                //[roomsArray addObject:tmp];
//            }
//        }
//    }
    
    [myTextField becomeFirstResponder];
}

-(void)yourTextFieldDoneButtonPressed {
    [myTextField resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TextField delegate
// placeholder position
- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, 10, 10);
}

// text position
- (CGRect)editingRectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, 10, 10);
}

- (BOOL) textField: (UITextField *)textField shouldChangeCharactersInRange: (NSRange)range replacementString: (NSString *)string {
    //changes on the keyboard "," to "." / Some keyboards have "," instead of "."
    NSString *input = [textField.text stringByReplacingCharactersInRange:range withString:string];
    input = [input stringByReplacingOccurrencesOfString:@"," withString:@"."];
    searchArray = [[NSMutableArray alloc] init];
    for (Room *room in roomsArray) {
        if ([[NSString stringWithFormat:@" %@",room.room_id] containsString:[NSString stringWithFormat:@" %@",input]]) {
            [searchArray addObject:room];
        }
    }
    
    if (input.length > 0) {
        searchActivated = YES;
    }
    [myTableView reloadData];
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - TableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (searchActivated) {
        return [searchArray count];
    }
    return [roomsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *MyIdentifier = @"MyIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:MyIdentifier];
    }
    
    if (searchActivated){
        cell.textLabel.text = [[searchArray objectAtIndex:indexPath.row] room_id];
        cell.detailTextLabel.text = F(@"%@", [self BuildingName:[[searchArray objectAtIndex:indexPath.row] building_id])];
        cell.detailTextLabel.textColor = [UIColor grayColor];
    } else {
        cell.textLabel.text = [[roomsArray objectAtIndex:indexPath.row] room_id];
        cell.detailTextLabel.text = F(@"%@", [self BuildingName:[[roomsArray objectAtIndex:indexPath.row] building_id]]);
        cell.detailTextLabel.textColor = [UIColor grayColor];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 44;
}

- (NSString *)BuildingName :(NSString *)buildingId {
    
    NSString *response = [NSString stringWithFormat:@""];
    
    if ([buildingId isEqualToString:@"mi"]) {
        response = [NSString stringWithFormat:@"TUM Bibliothek Mathematik & Informatik"];
    } else if ([buildingId isEqualToString:@"mw"]) {
        response = [NSString stringWithFormat:@"TUM, Fakultät für Maschinenwesen"];
    } else if ([buildingId isEqualToString:@"mc"]) {
        response = [NSString stringWithFormat:@"TUM Main Campus"];
    }
    
    return response;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // go back to the prev view if it comes from mapRoute
    Room *result = [[Room alloc] init];

    if (searchActivated) {
        result = [searchArray objectAtIndex:indexPath.row];
    } else{
        result = [roomsArray objectAtIndex:indexPath.row];
    }
    
    if (self.comesFromRouteSelection) {
        [self.delegate didDestionationSelection:result :self.option];
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"topSearchDataReceived" object:result userInfo:nil];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (IBAction)backButtonAction:(id)sender {
    if (self.comesFromRouteSelection) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

@end
