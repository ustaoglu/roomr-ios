//
//  ImageHandler.h
//  Pedometer
//
//  Created by HiWi on 04.06.18.
//  Copyright © 2018 Navindo GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

@interface ImageHandler : GMSSyncTileLayer

- (UIImage *)tileForX:(NSUInteger)x y:(NSUInteger)y zoom:(NSUInteger)zoom;

@end
