//
//  ViewController.m
//  Pedometer
//
//  Created by Efdal Ustaoglu on 05.01.18.
//  Copyright © 2018 Ariadne TUM. All rights reserved.
//

#import "ViewController.h"

@import CoreMotion;
@interface ViewController () {
    
    CMPedometer *my_pedometer;
    NSDate *app_start_date;
    NSTimer *myTimer;
    NSString *longitude;
    NSString *latitude;
//    MGLMapView *mapView;
    
    NSMutableArray * targetPoints;
    NSMutableArray * targetPointsTagged;
    NSMutableArray * routablePath;
    NSMutableArray * gyroscopeArray;
    
    NSMutableArray * corridors;
    NSMutableArray * corridorsInLevels;
    NSMutableArray * steps;
    NSMutableArray * stepsInLevels;
    LatLngWithTags *startingPoint;
    int drawStyle;
    NSDictionary *geoJSON;
    CLLocation *userCurrLoc;
    CLLocation *uniEntrance;
    
    MKPointAnnotation *userAnnotation;
    float usersCurrentDirectionValue;
    NSDate *lastCommitDate;
    NSDate *lastCommitDate2;
    
    NSTimer *posTimer;
    NSTimer *gyroTimer;
    
    NSDictionary* localInfo;
    
    NSMutableDictionary *dict_estimated_test_data;
    
    MKPointAnnotation *estimatedPosAnno;
    MKPointAnnotation *deadreckPosAnno;
    CLLocation *estimatedPos;
    CLLocation *deadreckPos;
    NSTimer *estPositionTimer;
}

@property (strong, nonatomic) CMPedometer *pedometer;
@property (strong, nonatomic) IBOutlet UILabel *stepsLabel;
@property (strong, nonatomic) IBOutlet UILabel *distanceLabel;
@property (strong, nonatomic) IBOutlet UILabel *cadenceLabel;
@property (strong, nonatomic) IBOutlet UILabel *paceLabel;
@property (strong, nonatomic) IBOutlet UILabel *flightsUpLabel;
@property (strong, nonatomic) IBOutlet UILabel *flightsDownLabel;
@end

@implementation ViewController
@synthesize myMapView, myTextField, roundedView, compassLabel;

- (void)viewDidLoad {
    
    lastCommitDate = [NSDate date];
    lastCommitDate2 = [NSDate date];
    self.ref = [[FIRDatabase database] reference];
    usersCurrentDirectionValue = 0;
    locUtil = [[LocationUtils alloc] init];
    
    my_pedometer = [[CMPedometer alloc] init];
    app_start_date = [NSDate date];
    
    targetPoints = [[NSMutableArray alloc] init];
    targetPointsTagged = [[NSMutableArray alloc] init];
    routablePath = [[NSMutableArray alloc] init];
    corridors = [[NSMutableArray alloc] init];
    corridorsInLevels = [[NSMutableArray alloc] init];
    steps = [[NSMutableArray alloc] init];
    stepsInLevels = [[NSMutableArray alloc] init];
    gyroscopeArray = [[NSMutableArray alloc] init];
    
    [self initMapElements];
    [self getCurrentLocation];
    [super viewDidLoad];
    [self resetLabels];
//    [self generateValue];
    
    //old approach when ble data receives send it
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(bleDictionary:) name:@"bleDictionary" object:nil];
    
}

- (void)initMapElements {
    
    roundedView.layer.cornerRadius = 5.0;
    roundedView.layer.borderColor  = [UIColor colorWithRed:0.74 green:0.16 blue:0.16 alpha:1.0].CGColor;
    roundedView.layer.borderWidth  = 2.0f;
    
    myTextField.layer.cornerRadius = 5.0;
    myTextField.layer.borderColor  = [UIColor colorWithRed:0.93 green:0.91 blue:0.91 alpha:1.0].CGColor;
    myTextField.layer.borderWidth  = 2.0f;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(donePressed:)];
    
    UIToolbar *numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, WIDTH(self.view), 44)];
    numberToolbar.tintColor = [UIColor colorWithRed:0.74 green:0.16 blue:0.16 alpha:1.0];
    [numberToolbar setItems:[NSArray arrayWithObjects:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], doneButton, nil]];
    myTextField.inputAccessoryView = numberToolbar;
}


- (void)showGeoJSON {

    startingPoint = [locUtil setupStartingPoint:@"48.26253058447" :@"11.66877060995" :@"0" :@"entrance"];//entrance
    
    uniEntrance = [[CLLocation alloc]initWithLatitude:[startingPoint.latlng.latitute doubleValue] longitude:[startingPoint.latlng.longitude doubleValue]];
    
//    NSURL *URL = [[NSBundle mainBundle] URLForResource:@"indoor_path_ga_mi" withExtension:@"geojson"];
//    NSURL *URL2 = [[NSBundle mainBundle] URLForResource:@"indoor_map_ga_mi_0" withExtension:@"geojson"];
    
    //munchner_freiheit
    NSURL *URL = [[NSBundle mainBundle] URLForResource:@"munchner_freiheit" withExtension:@"geojson"];
    NSURL *URL2 = [[NSBundle mainBundle] URLForResource:@"munchner_freiheit" withExtension:@"geojson"];
    
    NSData *data = [NSData dataWithContentsOfURL:URL];
    NSData *data2 = [NSData dataWithContentsOfURL:URL2];
    geoJSON = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSDictionary *miStructure = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
    NSArray *shapes = [GeoJSONSerialization shapesFromGeoJSONFeatureCollection:miStructure error:nil];
    drawStyle = DrawMap;

    for (MKShape *shape in shapes) {
        if ([shape isKindOfClass:[MKPointAnnotation class]]) {
            //[myMapView addAnnotation:shape];
        } else if ([shape conformsToProtocol:@protocol(MKOverlay)]) {
            [myMapView addOverlay:(id <MKOverlay>)shape];
        }
    }
}

#pragma mark - Map Functions

- (void)createMap {
    
    //    NSURL *url = [NSURL URLWithString:@"mapbox://styles/mapbox/streets-v10"];
    //    mapView = [[MGLMapView alloc] initWithFrame:self.view.bounds styleURL:url];
    //    mapView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    //    [mapView setCenterCoordinate:CLLocationCoordinate2DMake([latitude floatValue], [longitude floatValue])
    //                       zoomLevel:13
    //                        animated:NO];
    //    [self.view addSubview:mapView];
    
    myMapView.delegate = self;
    [self showGeoJSON];
    [self mapZoom];
    
    UIView *roundedViewUserLoc = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    roundedViewUserLoc.backgroundColor = [UIColor redColor];
    
    UITapGestureRecognizer *tgr = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleGesture:)];
    tgr.numberOfTapsRequired = 1;
    [myMapView addGestureRecognizer:tgr];
        //    [myMapView addOverlay:(id <MKOverlay>)shape];
}

- (void)handleGesture:(UIGestureRecognizer *)gestureRecognizer {
    
    if (gestureRecognizer.state != UIGestureRecognizerStateEnded)
        return;
    
    CGPoint touchPoint = [gestureRecognizer locationInView:myMapView];
    CLLocationCoordinate2D coordinate = [myMapView convertPoint:touchPoint toCoordinateFromView:myMapView];
    
    NSLog(@"latitude  %f longitude %f",coordinate.latitude,coordinate.longitude);

    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"" message:@"Do you want me set your position here?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        uniEntrance = [[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    }];
    UIAlertAction *noButton = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) { }];
    [alert addAction:okButton];
    [alert addAction:noButton];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)getCurrentLocation {
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager requestWhenInUseAuthorization];
    [locationManager startUpdatingLocation];
}


#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
//    UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        userCurrLoc = currentLocation;
        [locationManager stopUpdatingLocation];
        longitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        latitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        [self createMap];
    }
}


- (IBAction)startTracking:(id)sender {
    
    locUtil.routablePath = routablePath;
    locUtil.targetPoints = targetPoints;
    locUtil.targetPointsTagged = targetPointsTagged;
    
    drawStyle = DrawRoute;
//    self.geoJSONDijkstra = [locUtil processIndoorPathFeatures:geoJSON :self.geoJSONDijkstra];
//    myMapView = [locUtil handleRouteRequest:myMapView :drawStyle :self.geoJSONDijkstra :startingPoint :myTextField.text];
    
    userAnnotation = [[MKPointAnnotation alloc] init];
    [userAnnotation setCoordinate:(uniEntrance.coordinate)];
    [myMapView addAnnotation:userAnnotation];
    [self updatePosition];
    
//    // start live tracking
//    [self.pedometer startPedometerUpdatesFromDate:[NSDate date] withHandler:^(CMPedometerData * _Nullable pedometerData, NSError * _Nullable error) {
//
//        dispatch_async(dispatch_get_main_queue(), ^{
//            // this block is called for each live update
//            [self updateLabels:pedometerData];
//        });
//    }];
}

- (void)updatePosition {

    if (uniEntrance != nil) {
        
        //set user's current direction //usersCurrentDirectionValue
        [self startCompass];
        
        //after 2 seconds later if it has a right value then drop it
        [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(stopCompass) userInfo:nil repeats:NO];
        
        //then drop it and use gyroscope value to add which direction he turns (add the degree of it)
        //it's already been called in stopCompass

        //then start counting the steps (for this you need to call the pedometer and as long as it returns values, you need to update the view. Because I cannot call the service every 1 second)
    }
}

/*
void updatePosition(float steps) {

    if (currentLatLng != null) {
        LatLng previousPosition = currentLatLng;
        ArrayList<LatLng> closestLine = new ArrayList<>();
        currentLatLng = calculateNewPosition(currentLatLng, steps);
        LatLng closestPoint = currentLatLng;
        double minDistance = Integer.MAX_VALUE;
 
        int index = 0;
        int minIndex = Integer.MAX_VALUE;
        for (LatLng point : interpolatedRoute) {
            double distance = SphericalUtil.computeDistanceBetween(currentLatLng, point);
            if (distance < minDistance) {
                minDistance = distance;
                closestPoint = point;
                minIndex = index;
            }
            index++;
        }
        currentLatLng = closestPoint;
 
        if (minIndex < interpolatedRoute.size() - 1) {
            double lineHeading = SphericalUtil.computeHeading(interpolatedRoute.get(minIndex), interpolatedRoute.get(minIndex + 1));
            double oppositelineHeading = SphericalUtil.computeHeading(interpolatedRoute.get(minIndex + 1), interpolatedRoute.get(minIndex));
            double lineHeadingIn360 = lineHeading;
            double oppositeLineHeadingIn360 = oppositelineHeading;
            double walkingAngleIn360 = walkingAngle;
            if (lineHeadingIn360 < 0) {
                lineHeadingIn360 += 360;
            }
            if (oppositeLineHeadingIn360 < 0) {
                oppositeLineHeadingIn360 += 360;
            }
            if (walkingAngleIn360 < 0) {
                walkingAngleIn360 += 360;
            }
 
            System.out.println("LineHeadingIn360 = " + lineHeadingIn360 + ", oppositeLineHeadingIn360 = " + oppositeLineHeadingIn360 + ", walkingAngleIn360 = " + walkingAngleIn360);
 
            if (Math.abs(lineHeadingIn360 - walkingAngleIn360) < 70.0 || Math.abs(lineHeadingIn360 - walkingAngleIn360) > 290.0) {
                walkingAngle = lineHeading;
                currentLatLng = SphericalUtil.computeOffset(previousPosition, steps * stepSize, walkingAngle);
                Log.i("Walking angle: ", "Line Heading: " + lineHeading);
            } else if (Math.abs(oppositeLineHeadingIn360 - walkingAngleIn360) < 70.0 || Math.abs(oppositeLineHeadingIn360 - walkingAngleIn360) > 290.0) {
                Log.i("Walking angle: ", "Opposite line heading: " + oppositelineHeading);
                walkingAngle = oppositelineHeading;
                currentLatLng = SphericalUtil.computeOffset(previousPosition, steps * stepSize, walkingAngle);
            } else {
                System.out.println("Heading: " + walkingAngle + ", lineHeading: " + lineHeading + ", oppositeLineHeading: " + oppositelineHeading + "!!!!!!!!!!!!!!!!!!!!");
            }
        }
        LatLng positionOnMap = new LatLng(currentLatLng.latitude, currentLatLng.longitude);
        if (positionMarker != null) {
            positionMarker.remove();
        }
        
        addPositionCircle(positionOnMap);
        LatLng target = findClosestPointOnPath(currentLatLng);
    }
}
*/

- (void)updateUserAnnotation {
    
    //make this function continious so, it will be called every 1 second (or less).
    //create a timer and call the function from there
    if (uniEntrance.coordinate.latitude != 0 && uniEntrance.coordinate.longitude != 0) {
        [myMapView removeAnnotation:userAnnotation];
        [userAnnotation setCoordinate:(uniEntrance.coordinate)];
        [myMapView addAnnotation:userAnnotation];
    }
}

- (IBAction)stopTracking:(id)sender {
    
    [self updateUserAnnotation];
    // stop live tracking
    [self.pedometer stopPedometerUpdates];
    //[self resetLabels];
}

- (void)resetLabels {
    
    // reset labels
    self.stepsLabel.text = @"Press Start to begin\nPedometer Tracking";
    self.distanceLabel.text = nil;
    self.cadenceLabel.text = nil;
    self.paceLabel.text = nil;
    self.flightsUpLabel.text = nil;
    self.flightsDownLabel.text = nil;
}

-(CLLocation*)locationForAngle:(float)angle fromCenterLocation:(CLLocation *)center withDistance:(float)distance {
    //angle must be in radians
    float longitude = distance * cosf(angle) - center.coordinate.longitude;
    float latitude = distance * sinf(angle) - center.coordinate.latitude;
    return [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
}

//Calculate New Lat/Lng from previous location, distance and angle (in radians)
- (CLLocationCoordinate2D) locationWithBearing:(float)bearing distance:(float)distanceMeters fromLocation:(CLLocationCoordinate2D)origin {
    CLLocationCoordinate2D target;
    const double distRadians = distanceMeters / (6372797.6); // earth radius in meters
    
    float lat1 = origin.latitude * M_PI / 180;
    float lon1 = origin.longitude * M_PI / 180;
    
    float lat2 = asin( sin(lat1) * cos(distRadians) + cos(lat1) * sin(distRadians) * cos(bearing));
    float lon2 = lon1 + atan2( sin(bearing) * sin(distRadians) * cos(lat1),
                              cos(distRadians) - sin(lat1) * sin(lat2) );
    
    target.latitude = lat2 * 180 / M_PI;
    target.longitude = lon2 * 180 / M_PI; // no need to normalize a heading in degrees to be within -179.999999° to 180.00000°
    
    return target;
}


- (void)updateLabels:(CMPedometerData *)pedometerData {
    
    //NSLog(@"Pedometer data:%@",pedometerData);
    
    if (pedometerData.numberOfSteps > 0) {
        
        CLLocationCoordinate2D coord = [self locationWithBearing:DEGREES_TO_RADIANS(usersCurrentDirectionValue) distance:((([pedometerData.numberOfSteps floatValue] - stepsTillNow ) * stepSize ) ) fromLocation:CLLocationCoordinate2DMake(uniEntrance.coordinate.latitude,uniEntrance.coordinate.longitude)];
        
        uniEntrance = [[CLLocation alloc] initWithLatitude:coord.latitude longitude:coord.longitude];

        //uniEntrance = [self locationForAngle:DEGREES_TO_RADIANS(usersCurrentDirectionValue) fromCenterLocation:uniEntrance withDistance:(([pedometerData.numberOfSteps floatValue] - stepsTillNow ) * stepSize / 10000)];
        stepsTillNow = [pedometerData.numberOfSteps floatValue];
        [self updateUserAnnotation];
        
        //location from given lat,lng
        //CLLocationCoordinate2D coord = CLLocationCoordinate2DMake([latLngInPath[1] doubleValue], [latLngInPath[0] doubleValue]);
        
        /* //distance b/w two location
        CLLocation *locA = [[CLLocation alloc] initWithLatitude:[[[latlngArray[i] latlng] latitute] doubleValue] longitude:[[[latlngArray[i] latlng] longitude] doubleValue]];
        
        CLLocation *locB = [[CLLocation alloc] initWithLatitude:[[[latlngArray[i+1] latlng] latitute] doubleValue] longitude:[[[latlngArray[i+1] latlng] longitude] doubleValue]];
        
        CLLocationDistance distance = [locA distanceFromLocation:locB];
        */
        
    }
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc]init];
    formatter.maximumFractionDigits = 2;
    
    // step counting
    if ([CMPedometer isStepCountingAvailable]) {
        self.stepsLabel.text = [NSString stringWithFormat:@"Steps walked: %@", [formatter stringFromNumber:pedometerData.numberOfSteps]];
    } else {
        self.stepsLabel.text = @"Step Counter not available.";
    }
    
    // distance
    if ([CMPedometer isDistanceAvailable]) {
        self.distanceLabel.text = [NSString stringWithFormat:@"Distance travelled: \n%@ meters", [formatter stringFromNumber:pedometerData.distance]];
    } else {
        self.distanceLabel.text = @"Distance estimate not available.";
    }
    
    // pace
    if ([CMPedometer isPaceAvailable] && pedometerData.currentPace) {
        self.paceLabel.text = [NSString stringWithFormat:@"Current Pace: \n%@ seconds per meter", [formatter stringFromNumber:pedometerData.currentPace]];
    } else {
        self.paceLabel.text = @"Pace not available.";
    }
    
    // cadence
    if ([CMPedometer isCadenceAvailable] && pedometerData.currentCadence) {
        self.cadenceLabel.text = [NSString stringWithFormat:@"Cadence: \n%@ steps per second", [formatter stringFromNumber: pedometerData.currentCadence]];
    } else {
        self.cadenceLabel.text = @"Cadence not available.";
    }
    
    // flights climbed
    if ([CMPedometer isFloorCountingAvailable] && pedometerData.floorsAscended) {
        self.flightsUpLabel.text = [NSString stringWithFormat:@"Floors ascended: %@", pedometerData.floorsAscended];
    } else {
        self.flightsUpLabel.text = @"Floors ascended\nnot available.";
    }
    
    if ([CMPedometer isFloorCountingAvailable] && pedometerData.floorsDescended) {
        self.flightsDownLabel.text =[NSString stringWithFormat:@"Floors descended: %@", pedometerData.floorsDescended];
    } else {
        self.flightsDownLabel.text = @"Floors descended\nnot available.";
    }

}

#pragma mark - Pedometer Functions

- (CMPedometer *)pedometer {
    
    if (!_pedometer) {
        _pedometer = [[CMPedometer alloc]init];
    }
    return _pedometer;
}

- (void)startPedometer {
    //every second tries to update pedometer data (but it doesn't b/c of apple's pedometer api)
    myTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                               target:self
                                             selector:@selector(PedometerEverSecondsTick)
                                             userInfo:nil
                                              repeats:YES];
    //every second STREAMS data to the cloud no matter how many ble data has
    posTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                               target:self
                                             selector:@selector(UploadFirebaseWithUserLatLng)
                                             userInfo:nil
                                              repeats:YES];
}

- (void)PedometerEverSecondsTick {
    
    if (!my_pedometer) {
        my_pedometer = [[CMPedometer alloc]init];
    }
    
    // retrieve data between dates
    [my_pedometer queryPedometerDataFromDate:app_start_date toDate:[NSDate date] withHandler:^(CMPedometerData * _Nullable pedometerData, NSError * _Nullable error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // this block is called for each live update
            [self updateLabels:pedometerData];
        });
    }];
}

#pragma mark - MKMapViewDelegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView
            viewForAnnotation:(id <MKAnnotation>)annotation
{
    static NSString * PinIdentifier = @"Pin";
    
    MKAnnotationView *annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:PinIdentifier];
    
    if (!annotationView) {
        annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:PinIdentifier];
        MKPinAnnotationView *aa = annotationView;
        if ([annotation.title isEqualToString:@"dead"]) {
            aa.pinTintColor = [UIColor grayColor];
            //annotationView.backgroundColor = [UIColor greenColor];
        } else if ([annotation.title isEqualToString:@"estimated"]) {
            aa.pinTintColor = [UIColor greenColor];
            //annotationView.backgroundColor = [UIColor grayColor];
        }
        annotationView = aa;
    };
    
    annotationView.hidden = ![annotation isKindOfClass:[MKPointAnnotation class]];
    
    return annotationView;
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView
            rendererForOverlay:(id <MKOverlay>)overlay
{
    MKOverlayRenderer *renderer = nil;
    if ([overlay isKindOfClass:[MKPolyline class]]) {
        renderer = [[MKPolylineRenderer alloc] initWithPolyline:(MKPolyline *)overlay];
        
        if (drawStyle == DrawMap) {
            ((MKPolylineRenderer *)renderer).strokeColor = [UIColor blackColor];
            ((MKPolylineRenderer *)renderer).lineWidth = 4.0f;
        } else if (drawStyle == DrawRoute) {
            ((MKPolylineRenderer *)renderer).strokeColor = [UIColor colorWithRed:0.74 green:0.16 blue:0.16 alpha:1.0];
            ((MKPolylineRenderer *)renderer).lineWidth = 3.0f;
        }
        
    } else if ([overlay isKindOfClass:[MKPolygon class]]) {
        renderer = [[MKPolygonRenderer alloc] initWithPolygon:(MKPolygon *)overlay];
        ((MKPolygonRenderer *)renderer).strokeColor = [UIColor blackColor];
        //((MKPolygonRenderer *)renderer).fillColor = [UIColor colorWithRed:1.0f green:0.0f blue:0.0f alpha:0.5f];
        ((MKPolygonRenderer *)renderer).lineWidth = 3.0f;
    }
    
    renderer.alpha = 0.5;
    
    return renderer;
}

- (void)mapZoom {
    
    CLLocationCoordinate2D myx;
    myx.longitude = userCurrLoc.coordinate.longitude;
    myx.latitude = userCurrLoc.coordinate.latitude;
    
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.003;
    span.longitudeDelta = 0.003;
    region.span = span;
    region.center = myx;
    
    [myMapView setRegion:region animated:YES];
    [myMapView regionThatFits:region];
}

#pragma mark - Compass Delegate

- (void)startCompass {
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.delegate = self;
    //Start the compass updates.
    [locationManager startUpdatingHeading];
    //[self startPedometer];//after starting
}

- (void)stopCompass {
    
    [locationManager stopUpdatingHeading];
    [self startGyroscope];
    [self startPedometer];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {
    
    float mHeading = newHeading.magneticHeading;
    usersCurrentDirectionValue = mHeading;
    [self updateDirection:usersCurrentDirectionValue];
}

- (void)updateDirection:(float)mHeading {
    
    if ((mHeading >= 339) || (mHeading <= 22)) {
        compassLabel.text = @"N";
    }else if ((mHeading > 23) && (mHeading <= 68)) {
        compassLabel.text = @"NE";
    }else if ((mHeading > 69) && (mHeading <= 113)) {
        compassLabel.text = @"E";
    }else if ((mHeading > 114) && (mHeading <= 158)) {
        compassLabel.text = @"SE";
    }else if ((mHeading > 159) && (mHeading <= 203)) {
        compassLabel.text = @"S";
    }else if ((mHeading > 204) && (mHeading <= 248)) {
        compassLabel.text = @"SW";
    }else if ((mHeading > 249) && (mHeading <= 293)) {
        compassLabel.text = @"W";
    }else if ((mHeading > 294) && (mHeading <= 338)) {
        compassLabel.text = @"NW";
    }
    compassLabel.text = [compassLabel.text stringByAppendingString:F(@" (%.2f)", mHeading)];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:YES];
    return YES;
}

- (void)donePressed:(UIBarButtonItem *)sender {
    myTextField.text = F(@"%@", [myTextField.text stringByReplacingOccurrencesOfString:@"," withString:@"."]);
    [self textFieldShouldReturn:myTextField];
}

#pragma mark - Gyroscope Functions

- (void)startGyroscope {
    
    currentAccelX = 0;
    currentAccelY = 0;
    currentAccelZ = 0;
    
    currentRotX = 0;
    currentRotY = 0;
    currentRotZ = 0;
    
    self.motionManager = [[CMMotionManager alloc] init];
    self.motionManager.accelerometerUpdateInterval = .1;
    self.motionManager.gyroUpdateInterval = .1;
    
//    [self.motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue currentQueue]
//                                             withHandler:^(CMAccelerometerData *accelerometerData, NSError *error) {
//                                                 [self outputAccelertionData:accelerometerData.acceleration];
//                                                 if(error){
//                                                     NSLog(@"%@", error);
//                                                 }
//                                             }];
    
    [self.motionManager startGyroUpdatesToQueue:[NSOperationQueue currentQueue]
                                    withHandler:^(CMGyroData *gyroData, NSError *error) {
                                        [self outputRotationData:gyroData.rotationRate];
                                    }];
    
    gyroTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                 target:self
                                               selector:@selector(gyroscopeDataIntoDirection)
                                               userInfo:nil
                                                repeats:YES];
    
}

-(void)outputAccelertionData:(CMAcceleration)acceleration {

    currentAccelX = acceleration.x;
    currentAccelY = acceleration.y;
    currentAccelZ = acceleration.z;
//    NSLog(@"outputAccelertionData\nX:%.2f\nY:%.2f\nZ:%.2f", currentAccelX, currentAccelY, currentAccelZ);
}

-(void)outputRotationData:(CMRotationRate)rotation{
    
    currentRotX = rotation.x;
    currentRotY = rotation.y;
    currentRotZ = rotation.z;
    //NSLog(@"outputRotationData\nZ:%.2f", currentRotZ);

    //this parts should be comment in /**/
    
    [gyroscopeArray addObject:F(@"%.2f", RADIANS_TO_DEGREES(rotation.z) * (-1))];
    
//    usersCurrentDirectionValue = usersCurrentDirectionValue + rotation.z;
//    usersCurrentDirectionValue = fmodf(usersCurrentDirectionValue, 360.0f);
//    usersCurrentDirectionValue = abs(usersCurrentDirectionValue);

    //add them into a list and every 1 second take it's mean, add it the current direction then refresh the list
    
//    NSLog(@"---->Degree :%.2f",usersCurrentDirectionValue);
}

- (void)gyroscopeDataIntoDirection {
    
    double total = 0;
    for (int i = 0; i < gyroscopeArray.count; i++) {//take all the values into account
        total += [[gyroscopeArray objectAtIndex:i] doubleValue];
    }
    
    total = total / gyroscopeArray.count;//mean
    usersCurrentDirectionValue = usersCurrentDirectionValue + total;//add it to the current value
    
    if (usersCurrentDirectionValue < 0) {
        usersCurrentDirectionValue = usersCurrentDirectionValue + 360;
        usersCurrentDirectionValue = fmodf(usersCurrentDirectionValue, 360.0f);//take the mod
    }
    
    gyroscopeArray = [[NSMutableArray alloc] init];
    
    [self updateDirection:usersCurrentDirectionValue];
}

#pragma mark - Helpers (Firebase,BLE,DateComparison)

- (void)UploadFirebaseWithUserLatLng {
    
    NSMutableDictionary *tmpDict = [[NSMutableDictionary alloc] init];
    [tmpDict setValue:F(@"%@", [NSNumber numberWithDouble:uniEntrance.coordinate.latitude]) forKey:@"lat"];
    [tmpDict setValue:F(@"%@", [NSNumber numberWithDouble:uniEntrance.coordinate.longitude]) forKey:@"lng"];
    [tmpDict setValue:F(@"%f", uniEntrance.altitude) forKey:@"z"];
    [tmpDict setValue:TimeStamp forKey:@"timestamp"];
    [tmpDict setValue:localInfo forKey:@"BLE-Datas"];
    
    NSMutableDictionary *saved_dict = [[NSMutableDictionary alloc] init];
    [saved_dict setValue:tmpDict forKey:@"position"];
    
    dict_estimated_test_data = [saved_dict bv_jsonStringWithPrettyPrint:YES];
    
    if ([self twoDatesDifference2]) {
        [self sendPosFunction];
    }
    
    [[[[_ref child:@"bluetooth"] child:F(@"bluetooth-%d", [[[NSUserDefaults standardUserDefaults] valueForKey:@"fireCount"] intValue])] childByAutoId] setValue:tmpDict];
    
    //[[[_ref child:F(@"bluetooth-%d", [[[NSUserDefaults standardUserDefaults] valueForKey:@"fireCount"] intValue])] childByAutoId] setValue:tmpDict];
}

- (void)bleDictionary:(NSNotification*)notification {
    
    if (notification) {
        NSDictionary* userInfo = notification.userInfo;
        // NSLog(@"userInfo:%@",userInfo);
        
        //uniEntrance.coordinate.latitude
        //uniEntrance.coordinate.longitude
        //uniEntrance.altitude
        //00.11.053
        if (userInfo.count > 0 && [self twoDatesDifference]) {
            
            localInfo = userInfo;//save the ble information, so that every 1 second it will stream
        }
    }
}

- (BOOL)twoDatesDifference2 {
    unsigned int unitFlags = NSCalendarUnitHour | NSCalendarUnitSecond | NSCalendarUnitMinute | NSCalendarUnitDay | NSCalendarUnitMonth;
    
    NSDateComponents *conversionInfo = [[NSCalendar currentCalendar] components:unitFlags fromDate:lastCommitDate2   toDate:[NSDate date]  options:0];
    
    //    int months = [conversionInfo month];
    //    int days = [conversionInfo day];
    //    int hours = [conversionInfo hour];
    int minutes = [conversionInfo minute];
    int seconds = [conversionInfo second];
    
    //NSLog(@"%d mins, %d seconds", minutes, seconds);b
    if (seconds >= 4) {//sending data is changed from 3 to 1 seconds
        lastCommitDate2 = [NSDate date];
        return TRUE;
    }
    
    return FALSE;
}

- (BOOL)twoDatesDifference {
    unsigned int unitFlags = NSCalendarUnitHour | NSCalendarUnitSecond | NSCalendarUnitMinute | NSCalendarUnitDay | NSCalendarUnitMonth;
    
    NSDateComponents *conversionInfo = [[NSCalendar currentCalendar] components:unitFlags fromDate:lastCommitDate   toDate:[NSDate date]  options:0];
    
//    int months = [conversionInfo month];
//    int days = [conversionInfo day];
//    int hours = [conversionInfo hour];
    int minutes = [conversionInfo minute];
    int seconds = [conversionInfo second];
    
    //NSLog(@"%d mins, %d seconds", minutes, seconds);b
    if (seconds >= 1) {//sending data is changed from 3 to 1 seconds
        lastCommitDate = [NSDate date];
        return TRUE;
    }
    
    return FALSE;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)increaseFirebaseParentAction:(id)sender {

    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    if ([pref valueForKey:@"fireCount"] == nil) {
        [pref setValue:@"1" forKey:@"fireCount"];
        [pref synchronize];
    } else {
        NSString *fireStatus = [pref valueForKey:@"fireCount"];
        [pref setValue:F(@"%d", ([fireStatus intValue] + 1) ) forKey:@"fireCount"];
        [pref synchronize];
    }
}

- (IBAction)sendPositionAction:(id)sender {
    [self sendPosFunction];
}

- (void)sendPosFunction {
    
    self.estimatedPositionLabel.text = @"";
    dict_estimated_test_data = @"{\"position\":{\"bluetoothDetailsList\":[{\"macAddress\":\"C7:93:CC:90:1D:83\",\"name\":\"Kontakt\",\"signalStrength\":-86},{\"macAddress\":\"F2:75:4F:E0:68:6A\",\"name\":\"Kontakt\",\"signalStrength\":-91}],\"lat\":48.16234324549216,\"lng\":11.586565351712132,\"timestamp\":1519830286189}}";
    
    NSLog(@"--->%@",dict_estimated_test_data);
    PositionEstimateService *posService = [[PositionEstimateService alloc] init];
    posService.delegate = self;
    [posService estimatePosition:dict_estimated_test_data];
}


-(void)outputReceived:(NSArray *)userInfo {
    
    NSLog(@"received output--->%@",userInfo);
    self.estimatedPositionLabel.text = F(@"Cluster: %@", [userInfo valueForKey:@"cluster-id"]);
    //create points on the map
    
    if (deadreckPosAnno) {
        [myMapView removeAnnotation:deadreckPosAnno];
    }
    if (estimatedPosAnno) {
        [myMapView removeAnnotation:estimatedPosAnno];
    }
    
    deadreckPosAnno = [[MKPointAnnotation alloc] init];
    estimatedPosAnno = [[MKPointAnnotation alloc] init];
    
    deadreckPosAnno.title = @"dead";
    estimatedPosAnno.title = @"estimated";
    
    estimatedPos = [[CLLocation alloc]initWithLatitude:[[userInfo valueForKey:@"predicted-lat"] doubleValue] longitude:[[userInfo valueForKey:@"predicted-lng"] doubleValue]];
    deadreckPos = [[CLLocation alloc]initWithLatitude:[[userInfo valueForKey:@"dead-reck-lat"] doubleValue] longitude:[[userInfo valueForKey:@"dead-reck-lng"] doubleValue]];
    
    [deadreckPosAnno setCoordinate:(deadreckPos.coordinate)];
    [estimatedPosAnno setCoordinate:(estimatedPos.coordinate)];
    
    [myMapView addAnnotation:deadreckPosAnno];
    [myMapView addAnnotation:estimatedPosAnno];
}

@end
