//
//  LatLngWithTags.h
//  Pedometer
//
//  Created by Efdal Ustaoglu on 10.12.17.
//  Copyright © 2018 Ariadne TUM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LatLng.h"
@interface LatLngWithTags : NSObject

@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) LatLng *latlng;
@property (nonatomic, strong) NSString *level;

- (NSString *)toString;
- (NSString *)getLevel;
- (NSString *)getIdentifier;

@end
